{{#if showBackToAccount}}
    <a href="/" class="profile-information-button-back">
        <i class="profile-information-button-back-icon"></i>
        {{translate 'Back to Account'}}
    </a>
{{/if}}

<section class="quick-order row">

    <header class="quick-order-header">
        <h1 class="quick-order-title">{{translate 'New Order'}}</h1>
    </header>

    <div data-type="alert-placeholder"></div>

        <div class="quick-order-column-labels row">
            <div class="quick-order-item-label col-md-4">
                {{translate 'Item'}} <span class="quick-order-required-label">{{translate '(Required)'}}</span>
            </div>
            <div class="quick-order-qty-label col-md-2">
                {{translate 'Quantity'}}
            </div>
            <div class="quick-order-item-details-label col-md-3">
                {{translate 'Item Details'}}
            </div>
            <div class="quick-order-remove-label col-md-2">
                {{translate 'Unit Price'}}
            </div>
            <div class="quick-order-remove-label col-md-1">
                {{translate 'Remove'}}
            </div>                       
        </div>

        <hr>

        <div data-view="QuickView.CollectionView"></div>

    <div class="quick-order-buttons row">
        {{#if proceedcheckout}} 
            <div class="col-md-4 col-xs-12"><button class="quick-order-addline-button" data-action="addEmptyLine">{{translate 'Add Line'}} <i class="quick-order-add-line"></i></button></div>        
             <div class="col-md-4 col-xs-12">
                <a href="#" data-hashtag="#" class="quick-order-addline-button quick-order-checkout" data-touchpoint="checkout" data-proced-to-checkout="true">
                    {{translate 'Proceed to Checkout'}}                    
                </a>
            </div>    
            <div class="col-md-4 col-xs-12"><button class="quick-order-add-to-cart-button" data-action="multiadd">{{translate 'Add to Cart'}}</button></div>
        {{else}}
            <div class="col-md-6"><button class="quick-order-addline-button" data-action="addEmptyLine">{{translate 'Add Line'}} <i class="quick-order-add-line"></i></button></div>                        
            <div class="col-md-6"><button class="quick-order-add-to-cart-button" data-action="multiadd">{{translate 'Add to Cart'}}</button></div>
        {{/if}}
    </div>

<i class="quick-order-add-line"></i>

</section>


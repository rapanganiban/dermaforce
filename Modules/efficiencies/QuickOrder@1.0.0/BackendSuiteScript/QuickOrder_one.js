/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

function onRequest(context) {
        var userInput = context.getParameter('userInput') 
        ,   filters = [['isinactive', 'is', 'F'],
                    'AND',
                    ['isonline', 'is', 'T'],
                     'AND',
                    ['matrix', 'is', 'F'],
                     'AND',
                    [['itemid', 'startswith', userInput],
                        'OR',
                    ['upccode', 'startswith', userInput]]
                    ]
        ,   columns = [           
            new nlobjSearchColumn('itemid'),
            new nlobjSearchColumn('type'),
            new nlobjSearchColumn('subtype'),
            new nlobjSearchColumn('parent'),
            new nlobjSearchColumn('storedisplayname')
        ]              
        ,   results = []
        ,   search = nlapiCreateSearch("item",null,columns);

        search.setFilterExpression(filters)
        var resultset = search.runSearch()
        ,   search_result = resultset.getResults(0, 1000);      
        results = _.map(search_result, function(r) {                
                return {
                    id: r.getId(),
                    itemid: r.getValue('itemid'),
                    type: r.getValue("type"),
                    subtype: r.getValue('subtype'),
                    parent: r.getValue('parent')
                };           
        });
nlapiLogExecution('error', 'error', JSON.stringify(results));
        response.setContentType('json');
        response.write(JSON.stringify(results));       
};
/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

define('QuickOrder.Line.View', [
    'Backbone',
    'ItemViews.Price.View',
    'Backbone.CompositeView',
    'quickorder_line.tpl',
    'underscore',
    'Handlebars'
], function QuickOrderLineView(
    Backbone,
    ItemViewsPriceView,
    BackboneCompositeView,
    quickorder_line_tpl,
    _,
    Handlebars
  ) {
    'use strict';
    return Backbone.View.extend({

        template: quickorder_line_tpl,
        initialize: function initialize() {
            BackboneCompositeView.add(this);

            this.noresults = !this.model.get('suggestedResults') ||
            (!this.model.get('suggestedResults').isNew() &&
            this.model.get('suggestedResults').length === 0);
            this.suggestions = this.model.get('suggestedResults');
            this.index = this.model.get('internalid');
            this.query = this.model.get('query');
            this.referenceline = this.model.get('referenceLine');
            this.addedToCart = this.model.get('addedToCart');
            this.addedToCartCode = this.model.get('cartCode');
        },
        childViews: {
            'Item.Price': function ItemPrice() {
                return new ItemViewsPriceView({model: this.model.get('item')});
            }
        },
        getContext: function getContext() {
            var item = this.model.get('item')
            ,   link_attributes = {
                    href: SC.ENVIRONMENT.siteSettings.wsdkcompleteloginurl+item.get('custitem_rsm_wpitemurl')
                };
            return {
                index: this.index,
                referenceline: this.referenceline,
                addedToCart: this.addedToCart,
                minQtyAlert: (item.get('quantity') < item.get('_minimumQuantity')) && this.addedToCartCode !== '',
                query: item.get("itemid") ? item.get("itemid") : this.query,
                quantity: item.get('quantity'),
                minquantityvalue: item.get('_minimumQuantity'),
                minquantity: (item.get('_minimumQuantity') > 1),
                isinstock: item.get('isinstock') && !!item.get('_isPurchasable'),
                name: item.get('storedisplayname2'),
                sku: item.get('itemid'),
                linkAttributes: item.get('custitem_rsm_wpitemurl') != "" ?  new Handlebars.SafeString(_.objectToAtrributes(link_attributes))  : item.get('_linkAttributes'),
                thumbnailURL: item.get('_thumbnail').url,
                thumbnailAltImageText: item.get('_thumbnail').altimagetext,
                isNavigable: !!item.get('_isPurchasable'),
                itemSelected: !!item.get('storedisplayname2'),
                resultsFound: !!this.suggestions.length && !item.get('storedisplayname2'),
                results: this.suggestions.map(function map(result) {
                    return {
                        internalid: result.get('internalid'),
                        name: result.get('storedisplayname2'),
                        sku: result.get('itemid'),
                        selected: result.get('itemid') == item.get("itemid")
                    };
                }),
                noResults: this.noresults,
                newline:  item.get("itemid") ? false : true, //this.model.get('suggestedResults').isNew(),
                upccode : item.get('upccode')
            };
        }

    });
});
{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}
<div class="download-history-line row">
    <div class="col-md-4"><span class="hidden-desktop">Name: </span><span>{{name}}</span></div>
    <div class="col-md-2"><span class="hidden-desktop">Size (KB): </span><span>{{size}}</span></div>
    <div class="col-md-2"><span class="hidden-desktop">Created: </span><span>{{created}}</span></div>
    <div class="col-md-2"><span class="hidden-desktop">File Type: </span><span>{{fileType}}</span></div>
    <div class="col-md-2">
        <div class="download-action">
            {{#if isImage}}
                <a data-action="download-file" >{{translate 'Download'}}</a>
            {{else}}
                <a href="{{url}}" target="_blank" >{{translate 'Download'}}</a>
            {{/if}}
        </div>
    </div>
    <div class="clear-separator"></div>
    <div class="col-md-2">
        <img src="{{image}}" class="img-responsive">
    </div>
    <div class="col-md-10"><span>{{description}}</span></div>
    <div class="clear-separator"></div>
</div>
<section class="list">
	<header>
		<h3><%= view.page_header %></h3>
		<a href="/" class="back-btn"><%= _('&lt; Back to Account').translate() %></a>
		<hr class="divider-small">
	</header>

	<div data-type="list-header-placeholder"></div>


<% if (view.collection.length) { %>

	<div class="row-fluid visible-desktop list-sub-header order-history-list-header">
		<div class="tablet-span2 span12">
			<div class="span1">
				<span><%= _('Number').translate() %></span>
			</div>
			<div class="span3">
				<span><%= _('Name').translate() %></span>
			</div>			
			<div class="span2">
				<span><%= _('License Code').translate() %></span>
			</div>
			<div class="span2">
				<span><%= _('Expiration').translate() %></span>
			</div>
			<div class="span2">
				<span><%= _('Remaining Downloads').translate() %></span>
			</div>			
			<div class="span2">
				<span><%= _('Download Link').translate() %></span>
			</div>
		</div>
	</div>

	<% view.collection.each(function (file) {
		var id = file.get('file')
		,	fileid = file.get('name');
	%>
			<div class="row-fluid list-row-non-mobile" data-action="navigate" data-id="<%= id %>">
				<div class="tablet-span3 span12">
					<div class="span1 transaction-summary-one-col-tablet">
						<span class="list-row-text id-record">
							<a href="#" data-id="<%= id %>">
								<%= _('File #<span class="tranid">$(0)</span>').translate(id) %>
							</a>
						</span>
					</div>
					<div class="span3 transaction-summary-one-col-tablet">
						<span class="list-row-text">
							<span class="hidden-desktop list-row-text-info">
								<%= _('Neme:').translate() %>
							</span>
							<span class="name">
								<%= _('$(0)').translate( fileid ) %>
							</span>
						</span>
					</div>
					<div class="span2 transaction-summary-one-col-tablet">
						<span class="list-row-text">
							<span class="hidden-desktop list-row-text-info">
								<%= _('License Code:').translate() %>
							</span>
							<span class="licensecode">
								<%= _('$(0)').translate( file.get('licensecode') ? file.get('licensecode') : '-' ) %>
							</span>
						</span>
					</div>
					<div class="span2 transaction-summary-one-col-tablet">
						<span class="list-row-text">
							<span class="hidden-desktop list-row-text-info">
								<%= _('Expiration:').translate() %>
							</span>
							<span class="expiration">
								<%= _('$(0)').translate( file.get('expiration') ) %>
							</span>
						</span>
					</div>
					<div class="span2 transaction-summary-one-col-tablet">
						<span class="list-row-text">
							<span class="hidden-desktop list-row-text-info">
								<%= _('Remaining Downloads:').translate() %>
							</span>
							<span class="remainingdownloads">
								<%= _('$(0)').translate( file.get('remainingdownloads') ) %>
							</span>
						</span>
					</div>
					<div class="span2 wrap-text transaction-summary-one-col-tablet">
						<span class="list-row-text">
							<a class="btn btn-primary" target="_blank" href="<%= file.get('downloadurl') %>">
								<%= _('Download').translate() %>
							</a>
						</span>
					</div>
				</div>
			</div>
	<% }); %>

	<% } else if (view.isLoading) { %>
		<p class="list-empty"><%= _('Loading...').translate() %></p>
	<% } else { %>
		<p class="list-empty"><%= _('No order were found').translate() %></p>
	<% } %>

</section>
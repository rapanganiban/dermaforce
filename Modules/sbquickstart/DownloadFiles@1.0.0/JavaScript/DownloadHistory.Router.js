// DownloadHistory.Router.js
// -----------------------
define(
	'DownloadHistory.Router'
,	[
		'DownloadHistory.View'
	,	'DownloadHistory.Collection'
	,	'DownloadHistory.Model'
	,	'Backbone'
	]
,	function (
		View
	,	Collection
	, 	Model
	)
{
		'use strict';

		return Backbone.Router.extend({

			routes: {
				'downloadhistory': 'downloadHistory'
			}

		,	initialize: function (application)
			{
				this.application = application;
			}

			// list download History
		,	downloadHistory: function (options)
			{
				var self = this;
				var model = new Model()
				model.fetch({
							data: {internalid: 'id'}
					}).done(function(data){

						options = (options) ? SC.Utils.parseUrlOptions(options) : {page: 1};

						options.page = options.page || 1;

						var collection = new Collection()
						,	view = new View({
								application: self.application
							,	page: options.page
							,	collection: collection
							,	categories : data
							});

						collection.on('reset', view.showContent, view);

						view.showContent();
					});
			}
	})
});

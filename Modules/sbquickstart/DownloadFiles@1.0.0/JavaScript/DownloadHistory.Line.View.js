/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderHistory
define('DownloadHistory.Line.View'
,	[	'download_history_line.tpl'

	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	,	"DownloadHistory.Model"
	]
,	function (
		download_history_line_tpl

	,	Backbone
	,	_
	,	jQuery
	,	DownloadHistoryModel
	)
{
	'use strict';

	//@class OrderHistory.List.View view list of orders @extend Backbone.View
	return  Backbone.View.extend({
		//@property {Function} template
		template: download_history_line_tpl

	,	events : {
		'click [data-action="download-file"]': 'downloadFile'
	}

	,	downloadFile : function(e){
		var model = new DownloadHistoryModel({ internalid: this.model.get('id') })
			model.save({data : {'internalid': this.model.get('id')}}).done(function(data){
					var a = jQuery("<a>")
				    .attr("href",data.url)
				    .attr("download", "img.png")
				    .appendTo("body");
					a[0].click();
					a.remove();
				});
		}

		//@method getContext @return OrderHistory.List.View.Context
	,	getContext: function ()
		{
			//@class OrderHistory.List.View.Context

			return {
				//@property {String} id
				id: this.model.get('id')
				//@property {String} name
			,	name: this.model.get('name')
				//@property {String} size
			,	size: this.model.get('documentsize')
				// @property {String} filetype
			,	fileType: this.model.get('filetype')
				// @property {String} url
			,	url: this.model.get('url')
				// @property {String} created
			,	created: this.model.get('created').split(" ")[0]
				// @property {Boolean} isImage
			,	isImage:  this.model.get('filetype').indexOf("IMAGE") > -1
				// @property {String} description
			,	description:  this.model.get('description')
				// @property {String} isImage
			,	image:  this.model.get('image') == "" ? '/core/media/media.nl?id=4331&c=4197972&h=cce74a27f3bee2296904' : this.model.get('image')

			};
		}
	});

});

// DownloadHistory.js
// -----------------
// Defines the Download History  module (Model, Collection, View, Router)
define(
	'DownloadHistory'
,	[
		'DownloadHistory.View'
	,	'DownloadHistory.Router'	
	,	'underscore'
	, 	'Utils'
	]
,	function (View,  Router, Profile)
	{
		'use strict';

				
		return	{
			View: View
			
		,	Router: Router
		
		,	MenuItems: {
				id: 'downloads'
			,	name: _('Documents').translate()
			,	index: 3
			,	url: 'downloadhistory'		
			} 
		
		,	mountToApp: function (application)
			{
				return new Router(application);
			}
		};
	}
);

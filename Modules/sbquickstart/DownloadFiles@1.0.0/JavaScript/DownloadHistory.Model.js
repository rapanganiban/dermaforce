// DownloadHistory.Model.js
// -----------------------
define('DownloadHistory.Model', ['Backbone', 'underscore',	'Utils'], function ( Backbone , _)
{
	'use strict';

	return Backbone.Model.extend(
	{
		urlRoot:  _.getAbsoluteUrl('services/Downloads.Service.ss')
	});
});

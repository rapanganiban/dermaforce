/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderHistory
define('DownloadHistory.View'
,	[	'DownloadHistory.Model'
	,	'SC.Configuration'
	,	'GlobalViews.Pagination.View'
	,	'GlobalViews.ShowingCurrent.View'
	,	'DownloadHistory.ListHeader.View'
	,	'Backbone.CompositeView'
	,	'Backbone.CollectionView'
	,	'DownloadHistory.Line.View'
	,	'Handlebars'

	,	'download_history.tpl'

	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	]
,	function (
		DownloadHistoryModel
	,	Configuration
	,	GlobalViewsPaginationView
	,	GlobalViewsShowingCurrentView
	,	ListHeaderView
	,	BackboneCompositeView
	,	BackboneCollectionView
	,	DownloadHistoryLineView
	,	Handlebars

	,	download_history_tpl

	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';

	//@class OrderHistory.List.View view list of orders @extend Backbone.View
	return  Backbone.View.extend({
		//@property {Function} template
		template: download_history_tpl
		//@property {String} title
	,	title: _('Documents').translate()
		//@property {String} className
	,	className: 'OrderListView'
		//@property {String} page_header
	,	page_header: _('Documents').translate()
		//@property {Object} attributes
	,	attributes: {
			'class': 'OrderListView'
		}

		//@method getSelectedMenu
	,	getSelectedMenu: function ()
		{
			return 'downloads';
		}
		//@method getBreadcrumbPages
	,	getBreadcrumbPages: function ()
		{
			return {
					text: this.title
				,	href: '/downloads'
			};
		}
		//@method initialize
	,	initialize: function (options)
		{
			this.application = options.application;
			this.collection = options.collection;

			var isoDate = _.dateToString(new Date());

			this.rangeFilterOptions = {
				fromMin: '1800-01-02'
			,	fromMax: isoDate
			,	toMin: '1800-01-02'
			,	toMax: isoDate
			};

			this.listenCollection();

			// Manages sorting and filtering of the collection
			this.listHeader = new ListHeaderView({
				view: this
			,	application: this.application
			,	collection: this.collection
			,	sorts: this.sortOptions
			,	categories : options.categories
			,	rangeFilter: 'date'
			,	rangeFilterLabel: _('From').translate()
			,	hidePagination: true
			});

			this.showStatusFilter = options.showStatusFilter;

			BackboneCompositeView.add(this);

			this.application.getLayout().on('afterAppendView', function (){
				if (_.parseUrlOptions(Backbone.history.fragment).document && _.parseUrlOptions(Backbone.history.fragment).document != "") {
                    var searchInput = $('#documentName');

                    var strLength = searchInput.val().length * 2;

                    searchInput.focus();
                    searchInput[0].setSelectionRange(strLength, strLength);
                }
            });
		}
		//@method navigateToOrder
	,	navigateToOrder: function (e)
		{
			//ignore clicks on anchors and buttons
			if (_.isTargetActionable(e))
			{
				return;
			}

			if (!jQuery(e.target).closest('[data-type="accordion"]').length)
			{
				var order_id = jQuery(e.target).closest('[data-id]').data('id')
				,	recordtype = jQuery(e.target).closest('[data-record-type]').data('record-type');
				Backbone.history.navigate('#purchases/view/' + recordtype + '/' + order_id, {trigger:true});
			}
		}
		//@method listenCollection
	,	listenCollection: function ()
		{
			this.setLoading(true);

			this.collection.on({
				request: jQuery.proxy(this, 'setLoading', true)
			,	reset: jQuery.proxy(this, 'setLoading', false)
			});
		}
		//@method setLoading
	,	setLoading: function (value)
		{
			this.isLoading = value;
		}
		//@property {Array} sortOptions
		// Array of default sort options
		// sorts only apply on the current collection
		// which might be a filtered version of the original
	,	sortOptions: [
			{
				value: 'created'
			,	name: _('Sort By Date').translate()
			}
		,	{
				value: 'name'
			,	name: _('Sort By Name').translate()
			,	selected: true
			}
		]
		//@property {Object} childViews
	,	childViews: {
			'ListHeader': function ()
			{
				return this.listHeader;
			}
		,	'GlobalViews.Pagination': function ()
			{
				return new GlobalViewsPaginationView(_.extend({
					totalPages: Math.ceil(this.collection.totalRecordsFound / this.collection.recordsPerPage)
				}, Configuration.defaultPaginationSettings));
			}
		,	'GlobalViews.ShowCurrentPage': function ()
			{
				return new GlobalViewsShowingCurrentView({
					items_per_page: this.collection.recordsPerPage
		 		,	total_items: this.collection.totalRecordsFound
				,	total_pages: Math.ceil(this.collection.totalRecordsFound / this.collection.recordsPerPage)
				});
			}
		,	'Downloads.Result': function ()
			{
				var self = this
				,	records_collection = new Backbone.Collection(this.collection.map(function (file)
					{
						var columns = [
							{
								label: _('Image:').translate()
							,	type: 'image'
							,	name: 'image'
							,	value: file.get('image')
							}
						,	{
								label: _('Name:').translate()
							,	type: 'text'
							,	name: 'name'
							,	value: file.get('name')
							}
						,	{
								label: _('Size:').translate()
							,	type: 'text'
							,	name: 'documentsize'
							,	value: file.get('documentsize')
							}
						,	{
								label: _('Type: (kb)').translate()
							,	type: 'text'
							,	name: 'filetype'
							,	value: file.get('filetype')
							}
						,	{
								label: _('URL:').translate()
							,	type: 'text'
							,	name: 'url'
							,	value: file.get('url')
							}
						,	{
								label: _('Description:').translate()
							,	type: 'text'
							,	name: 'description'
							,	value: file.get('description')
							}
						];

						var model = new Backbone.Model({
								//@property {Integer} id
								id: file.get('id')
								//@property {String} name
							,	name: file.get('name')
								//@property {String} size
							,	documentsize: file.get('documentsize')
								// @property {String} filetype
							,	filetype: file.get('filetype')
								// @property {String} url
							,	url: file.get('url')
								// @property {String} date
							,	created: file.get('created')
								// @property {String} image
							,	image: file.get('image')
								// @property {String} description
							,	description: file.get('description')

							,	columns: columns
							});

						return model;
					}));

				return new BackboneCollectionView({
					childView: DownloadHistoryLineView
				,	collection: records_collection
				,	viewsPerRow: 1
				});
			}
		}

		//@method getContext @return OrderHistory.List.View.Context
	,	getContext: function ()
		{
			//@class OrderHistory.List.View.Context
			return {
				//@property {String} pageHeader
				pageHeader: this.page_header
				//@property {Boolean} collectionLengthGreaterThan0
			,	collectionLengthGreaterThan0: this.collection.length > 0
				//@property {Boolean} isLoading
			,	isLoading: this.isLoading
				// @property {Boolean} showPagination
			,	showPagination: !!(this.collection.totalRecordsFound && this.collection.recordsPerPage)
				// @property {Boolean} showCurrentPage
			,	showCurrentPage: this.options.showCurrentPage
				//@property {Boolean} showBackToAccount
			,	showBackToAccount: Configuration.get('siteSettings.sitetype') === 'STANDARD'
				//@property {Boolean} showStatusFilter
			,	showStatusFilter: this.showStatusFilter || false
			};
		}
	});

});

// DownloadHistory.Model.js
// -----------------------
define('DownloadHistory.Line.Model', ['Backbone'], function ( Backbone )
{
	'use strict';
	
	return Backbone.Model.extend(
	{
		urlRoot: _.getAbsoluteUrl('/checkout/services/Downloads.Service.ss')
	});
});

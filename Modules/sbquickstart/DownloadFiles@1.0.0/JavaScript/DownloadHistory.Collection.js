// DownloadHistory.Collection.js
// -----------------------
// Download History collection
define('DownloadHistory.Collection'
,	[
		'DownloadHistory.Model'
	,	'Backbone'
	]
, function (
		Model
	,	Backbone
)
{
	'use strict';

	return Backbone.Collection.extend(
	{
		model: Model

	,	url: _.getAbsoluteUrl('services/Downloads.Service.ss')

	,	parse: function (response)
		{
			this.totalRecordsFound = response.totalRecordsFound;
			this.recordsPerPage = response.recordsPerPage;

			return response.records;
		}

	,	update: function (options)
		{
			var range = options.range || {};

			this.fetch({
				data: {
					filter: options.filter && options.filter.value
				,	sort: options.sort.value
				,	order: options.order
				,	from: range.from ? new Date(range.from.replace(/-/g,'/')).getTime() : null
				,	to: range.to ? new Date(range.to.replace(/-/g,'/')).getTime() : null
				,	page: options.page
				,	docname: options.document != "" && options.document?  options.document : null
				,	category: options.category != "" && options.category?  options.category : null
				}

			,	reset: true
			,	killerId: options.killerId
			});
		}

	});
});

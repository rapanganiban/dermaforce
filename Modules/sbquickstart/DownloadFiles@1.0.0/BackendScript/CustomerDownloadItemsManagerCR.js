var downloadsSublist = 'download';

function service(request, response) {
    var method = request.getParameter('method') || 'retrieveDownloads';

    switch (method) {
        case 'download':
            download(request, response);
            break;
        case 'retrieveDownloads':
        default:
            retrieveDownloads(request, response);
    }
}

/**
 * retrieve the downloads sublist file id for the customer logged in
 */
function retrieveDownloads(request, response) {
    var customer;
    var downloadsCount;
    var objResponse = {};
    var cid    = nlapiGetUser() > 0? nlapiGetUser() : request.getParameter('cid');
    var documentname = request.getParameter('docname')
    var category = request.getParameter('category')
    var paramenters = request.getAllParameters();

    try {

        var filters =   [ new nlobjSearchFilter('custrecord_sc_cr_customer', null, 'is', cid).setOr(true),
            new nlobjSearchFilter('custrecord_sc_cr_apply_to_all', null, 'is', 'T')];

        if(category && category != "all" && category != null){
            filters.push( new nlobjSearchFilter('custrecord_sc_cf_category', null, 'is', category) )
        }

        var columns = [
            new nlobjSearchColumn("custrecord_sc_cr_files"),
            new nlobjSearchColumn("custrecord_sc_cr_description"),
            new nlobjSearchColumn("custrecord_sc_cr_display_image")
        ]
        filesid = [],
            filesidDesc = [],
            filesIdSearch = filesIdSearch = getPaginatedSearchResults({page : 1, filters : filters, columns : columns, record_type : "customrecord_sc_customer_files", results_per_page : 1000})
        if(filesIdSearch.records && filesIdSearch.records.length )
        {
            nlapiLogExecution('ERROR', 'filesIdSearch.records.length ', filesIdSearch.records.length );
            _.each(filesIdSearch.records, function(files){
                files = JSON.stringify(files);
                files = JSON.parse(files)
                filesid.push(files.columns.custrecord_sc_cr_files.internalid)

                if(documentname && files.columns.custrecord_sc_cr_description){
                    var lowName = documentname.toLowerCase(),
                        lowDesc = files.columns.custrecord_sc_cr_description.toLowerCase();
                    if(lowDesc.indexOf(lowName) > -1) {
                        filesidDesc.push(files.columns.custrecord_sc_cr_files.internalid)
                    }
                }
            });

            var filters =   []
                ,   columns =   [
                new nlobjSearchColumn('internalid')
                ,   new nlobjSearchColumn('name')
                ,   new nlobjSearchColumn('folder')
                ,   new nlobjSearchColumn('documentsize')
                ,   new nlobjSearchColumn('filetype')
                ,   new nlobjSearchColumn('url')
                ,   new nlobjSearchColumn('created')
            ];
            filters.push(['internalid', 'anyof', filesid]);
            if( paramenters.from && paramenters.to )
            {
                var offset = new Date().getTimezoneOffset() * 60 * 1000;
                filters.push("AND");
                filters.push(['created', 'within', new Date( parseInt( paramenters.from, 10 ) + offset ), new Date( parseInt( paramenters.to, 10 ) + offset )]);
            }

            if( paramenters.sort )
            {
                columns = _.union(columns, [
                    new nlobjSearchColumn( paramenters.sort).setSort( ( paramenters.order == 1 ) )
                ]);
            }

            if( documentname )
            {
                filters.push("AND");
                if(filesidDesc.length > 0){
                    expression = [
                        ['name', 'contains', documentname]
                        ,'OR'
                        ,[ 'internalid', 'anyof', filesidDesc]
                    ]
                    filters.push(expression);
                }else{
                    filters.push(['name', 'contains', documentname]);
                }
            }

            var files = getPaginatedSearchResults({page : paramenters.page, filters : filters, columns : columns, record_type : "file", results_per_page : 10})
            nlapiLogExecution("ERROR","files",JSON.stringify(files))
            if(files.records && files.records.length )
            {
                objResponse = {
                    totalRecordsFound : files.totalRecordsFound,
                    page :  parseInt(files.page),
                    recordsPerPage: parseInt(files.recordsPerPage)
                }

                objResponse.records =   _.map(files.records, function(file)
                {
                    var filedata = _.find(filesIdSearch.records, function(files){
                        files = JSON.stringify(files);
                        files = JSON.parse(files)
                        return files.columns.custrecord_sc_cr_files.internalid== file.getValue('internalid');
                    });
                    return {
                        id : file.getValue('internalid'),
                        name: file.getValue('name'),
                        folder: file.getValue('folder'),
                        documentsize: file.getValue('documentsize'),
                        filetype: file.getValue('filetype'),
                        url: file.getValue('url'),
                        created: file.getValue('created'),
                        description : filedata.getValue("custrecord_sc_cr_description"),
                        image : filedata.getValue("custrecord_sc_cr_display_image") && filedata.getValue("custrecord_sc_cr_display_image") != null ? nlapiLookupField("file",filedata.getValue("custrecord_sc_cr_display_image"),'URL') : ""
                    };
                });
            }

        }

    } catch (e) {
        nlapiLogExecution('error', 'error', e);
    }

    response.setContentType('json');
    response.write(JSON.stringify(objResponse));
}

/**
 * returns true if the date passed as parameter is from the past
 */
function isExpired(date) {
    // 23:59:59 to milliseconds
    var time = (23 * 60 * 60 * 1000) + (59 * 60 * 1000) + (59 * 1000);
    var expirationDate = new Date(date).getTime() + time;

    return expirationDate < Date.now();
}

/**
 * check if the current logged in user has the file requested inside his download sublist
 */
function canDownload(fileID) {

    var cid = nlapiGetUser() > 0? nlapiGetUser() : request.getParameter('cid');

    try {

        var file = nlapiSearchRecord(
            'customrecord_sc_customer_files'
            ,   null
            ,   [
                new nlobjSearchFilter('custrecord_sc_cr_customer', null, 'is', cid).setOr(true)
                ,   new nlobjSearchFilter('custrecord_sc_cr_apply_to_all', null, 'is', 'T')
            ]
            ,  [ new nlobjSearchColumn("custrecord_sc_cr_files")]
        );
        var fileFound = _.find(file, function(files){return files.getValue("custrecord_sc_cr_files") == fileID})

        return fileFound ? fileFound :  null;

    } catch (e) {
        nlapiLogExecution('error', 'error', e);
    }

}

function download(request, response) {
    var fileID = request.getParameter('fileID');

    var cid    = nlapiGetUser() > 0? nlapiGetUser() : request.getParameter('cid');
    nlapiLogExecution('ERROR', 'cid', cid);
    var canDownloadData = canDownload(fileID);

    if ( canDownloadData ) {

        var file = nlapiLoadFile(fileID),
            name = file.getName(),
            type = file.getType(),
            url  = file.getURL();

        if(type.indexOf("IMAGE") > -1 || type == "ICON"){
            response.setContentType('JSON');
            response.write(JSON.stringify({url:url}));
        }else{
            response.setContentType(file.getType(), name, 'attachment');
            response.write(file);

        }



    } else {
        response.write('You can not download this file');
    }
}


function getPaginatedSearchResults (options)
{
    options = options || {};

    var results_per_page = options.results_per_page
        ,   page = options.page || 1
        ,   columns = options.columns || []
        ,   filters = options.filters || []
        ,   record_type = options.record_type
        ,   range_start = (page * results_per_page) - results_per_page
        ,   range_end = page * results_per_page
        ,   result = {
        page: page
        ,   recordsPerPage: results_per_page
        ,   records: []
    };

    var column_count = options.column_count || new nlobjSearchColumn('internalid', null, 'count')
        ,   count_result = nlapiSearchRecord(record_type, null, filters, [column_count]);

    result.totalRecordsFound = parseInt(count_result[0].getValue(column_count), 10);



    var search = nlapiCreateSearch(record_type, filters, columns).runSearch();
    result.records = search.getResults(range_start, range_end);




    return result;
}
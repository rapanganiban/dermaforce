var downloadsSublist = 'download';

function service(request, response) {
    var method = request.getParameter('method') || 'retrieveDownloads';

    switch (method) {
        case 'download':
            download(request, response);
            break;
        case 'retrieveDownloads':
        default:
            retrieveDownloads(request, response);
    }
}

/**
 * retrieve the downloads sublist file id for the customer logged in
 */
function retrieveDownloads(request, response) {
    var customer;
    var downloadsCount;
    var objResponse = {};
    var cid    = nlapiGetUser() > 0? nlapiGetUser() : request.getParameter('cid');
    var paramenters = request.getAllParameters();

    try {

        var filters =   [ new nlobjSearchFilter('internalid', null, 'is', cid) ]
        ,   columns =   [
                            new nlobjSearchColumn('internalid')
                        ,   new nlobjSearchColumn('internalid', 'file')
                        ,   new nlobjSearchColumn('name', 'file')
                        ,   new nlobjSearchColumn('folder', 'file')
                        ,   new nlobjSearchColumn('documentsize', 'file')
                        ,   new nlobjSearchColumn('filetype', 'file')
                        ,   new nlobjSearchColumn('url', 'file')
                        ,   new nlobjSearchColumn('created', 'file')
                        ];

        if( paramenters.from && paramenters.to )
        {
            var offset = new Date().getTimezoneOffset() * 60 * 1000;

            filters = _.union(filters, [
                new nlobjSearchFilter('created', 'file', 'within', new Date( parseInt( paramenters.from, 10 ) + offset ), new Date( parseInt( paramenters.to, 10 ) + offset ) )
            ]);
        }

        if( paramenters.sort )
        {            
            columns = _.union(columns, [
                new nlobjSearchColumn( paramenters.sort, 'file' ).setSort( ( paramenters.order == 1 ) )
            ]);
        }

        var files = nlapiSearchRecord(
                        'customer'
                    ,   null
                    ,   filters
                    ,   columns
                    );

        if(files && files.length )
        {
            objResponse.totalRecordsFound = files.length;

            objResponse.records =   _.map(files, function(file)
                                    {
                                        return {
                                            id : file.getValue('internalid', 'file'),
                                            name: file.getValue('name', 'file'),
                                            folder: file.getValue('folder', 'file'),
                                            documentsize: file.getValue('documentsize', 'file'),
                                            filetype: file.getValue('filetype', 'file'),
                                            url: file.getValue('url', 'file'),
                                            created: file.getValue('created', 'file')
                                        };
                                    });
        }
       
    } catch (e) {
        nlapiLogExecution('error', 'error', e);
    }

    response.setContentType('json');
    response.write(JSON.stringify(objResponse));
}

/**
 * returns true if the date passed as parameter is from the past
 */
function isExpired(date) {
    // 23:59:59 to milliseconds
    var time = (23 * 60 * 60 * 1000) + (59 * 60 * 1000) + (59 * 1000);
    var expirationDate = new Date(date).getTime() + time;

    return expirationDate < Date.now();
}

/**
 * check if the current logged in user has the file requested inside his download sublist
 */
function canDownload(fileID) {
    
    var cid = nlapiGetUser() > 0? nlapiGetUser() : request.getParameter('cid');

    try {
        var file = nlapiSearchRecord(
                        'customer'
                    ,   null
                    ,   [
                            new nlobjSearchFilter('internalid', null, 'is', cid)
                        ,   new nlobjSearchFilter('internalid', 'file', 'is', fileID)
                        ]
                    ,   [
                            new nlobjSearchColumn('internalid')
                        ,   new nlobjSearchColumn('internalid', 'file')
                        ,   new nlobjSearchColumn('name', 'file')
                        ,   new nlobjSearchColumn('folder', 'file')
                        ,   new nlobjSearchColumn('documentsize', 'file')
                        ,   new nlobjSearchColumn('filetype', 'file')
                        ,   new nlobjSearchColumn('url', 'file')
                        ,   new nlobjSearchColumn('created', 'file')
                        ]
                    );

        return file && file.length ? file[0] :  null;

    } catch (e) {
        nlapiLogExecution('error', 'error', e);
    }

}

function download(request, response) {
    var fileID = request.getParameter('fileID');
    var cid    = nlapiGetUser() > 0? nlapiGetUser() : request.getParameter('cid');
    var canDownloadData = canDownload(fileID);

    if ( canDownloadData ) {
        
        var file = nlapiLoadFile(fileID);
        response.setContentType(file.getType(), null, 'attachment');
        response.write(file);

        /*
        // remove 1 from remainingDownloads
        var customer = nlapiLoadRecord('customer', nlapiGetUser() > 0? nlapiGetUser() : cid );
        var remainingDownloads = customer.getLineItemValue(downloadsSublist, 'remainingdownloads', canDownloadData.index);

        customer.setLineItemValue(downloadsSublist, 'remainingdownloads', canDownloadData.index, (Number(remainingDownloads) - 1));
        nlapiSubmitRecord(customer);
        */
        
    } else {
        response.write('You can not download this file');
    }
}

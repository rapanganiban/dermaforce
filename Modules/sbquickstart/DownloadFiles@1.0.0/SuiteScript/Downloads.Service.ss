/*exported service*/
// placed-order.ss
// ----------------
// Service to manage orders requests
function service (request)
{
	'use strict';
	// Application is defined in ssp library commons.js
	var Application = require('Application');

	try
	{
		//Only can get an order if you are logged in
		if (session.isLoggedIn2())
		{

			var method = request.getMethod()
			//  Order model is defined on ssp library Models.js
			,	DownloadModel = require('Download.Model')
			,	data = JSON.parse(request.getBody() || '{}')
			,	id = request.getParameter('internalid') || data.internalid;

			if (Application.getPermissions().transactions.tranSalesOrd > 0)
			{
				switch (method)
				{
					case 'GET':
							if(id && id != ""){
									Application.sendContent(DownloadModel.getCategoryList());
							}else{
								//If the id exist, sends the response of Order.get(id), else sends the response of (Order.list(options) || [])
								Application.sendContent(DownloadModel.list({
									filter: request.getParameter('filter')
								,	order: request.getParameter('order')
								,	sort: request.getParameter('sort')
								,	from: request.getParameter('from')
								,	to: request.getParameter('to')
								,	page: request.getParameter('page') || 1
								,	docname:  request.getParameter('docname')
								,	category:  request.getParameter('category')
								,	results_per_page: request.getParameter('results_per_page')
								}));
							}

					break;

					case 'PUT':

							Application.sendContent(DownloadModel.get(id));
					break;

					default:
						// methodNotAllowedError is defined in ssp library commons.js
						Application.sendError(methodNotAllowedError);
				}
			}
			else
			{
				// forbiddenError is defined in ssp library commons.js
				Application.sendError(forbiddenError);
			}
		}
		else
		{
			// unauthorizedError is defined in ssp library commons.js
			Application.sendError(unauthorizedError);
		}
	}
	catch (e)
	{
		Application.sendError(e);
	}
}

//Model.js
// PlacedOrder.js
// ----------
// Handles fetching orders

/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

define('Download.Model',
	[
    	'SC.Model'
   	,	'underscore'
	]
, function DownloadModel(
    SCModel,
    _
) {
    'use strict';

    return SCModel.extend({

        name: 'Download'

	,	list: function (data)
		{
			data = data || {};

			_.extend(data,{
				method : 'retrieveDownloads'
			,	cid : nlapiGetUser()
			})

			var url = nlapiResolveURL('SUITELET', 'customscript_download_management_cust_f', 'customdeploy_download_management_cust_f', true)
			,	response = nlapiRequestURL(url, data)
			,	result   = {};

			if(response.getBody())
			{
				result = JSON.parse( response.getBody() );

				//Generate download url
				_.each(result.records, function(file)
				{
					file.url = url +'&method=download'+ '&fileID='+file.id +'&cid=' + nlapiGetUser();
				});

			}

			return result;
		}

		,	get: function (id)
		{
			var data = data || {};

			_.extend(data,{
				method : 'download'
			,	fileID : id
			,	cid : nlapiGetUser()
			})

			var url = nlapiResolveURL('SUITELET', 'customscript_download_management_cust_f', 'customdeploy_download_management_cust_f', true)
			,	response = nlapiRequestURL(url, data)
			,	result   = "";

			if(response.getBody())
			{
				result =  JSON.parse( response.getBody() );
			}

			return result;
		}

		, getCategoryList: function(){

			var record = nlapiLoadRecord('customlist','100'),
			 	count = record.getLineItemCount('customvalue'),
				result = [{
							value: 'all'
						,	name: '- All Categories -'
						,	selected: true
						}];
			for(var i = 1; i<=count; i++) {
			 	var listItem= {
					name : record.getLineItemValue('customvalue','value',i),
			  		value : record.getLineItemValue('customvalue','valueid',i)
				}
				result.push(listItem)
			}
			return result;
		}
    });

});

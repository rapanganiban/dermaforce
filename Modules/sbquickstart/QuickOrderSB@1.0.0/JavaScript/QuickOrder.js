/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

// @module QuickOrder
define('QuickOrder', [
    'QuickOrder.Router',
    'QuickOrder.Configuration',   
    'underscore',
    'Utils'
], function QuickOrder(
    Router,
    Configuration
) {
    'use strict';
    
    
    
    return  {
        
        MenuItems:  {
            parent : 'orders'
        ,   id: 'quickorder'
        ,   name: _('New Order').translate()
        ,   url: 'quickorder'
        ,   index: 1
        },
        mountToApp: function mountToApp(application) {
            application.Configuration.quickOrder = Configuration;
            return new Router(application);
        }
    };
});

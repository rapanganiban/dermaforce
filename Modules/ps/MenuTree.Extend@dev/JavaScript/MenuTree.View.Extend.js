/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module MenuTree
define('MenuTree.View.Extend'
,	[
		'MenuTree.View'
	,	'Profile.Model'
	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	]
,	function(

		MenuTreeView
	
	,	ProfileModel
	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';
	// @class MenuTree.View Implements the tree menu items that is present in MyAccount. It's a Singleton. @extends Backbone.View
	return _.extend(MenuTreeView.prototype, {


		render: function()
		{
			var self = this
			ProfileModel.getPromise().done(function(profile){
				if(profile.isperson){					
					self.removeMenuItem("reporting")
					self.removeMenuItem("customerinvite")
					self.removeMenuItem("downloads")
					self.removeMenuItem("quickorder")
					self.removeMenuItem("reportingtotalcompany")					
				}
				self.fixedMenuItems = self.getFixedMenuItems();
				Backbone.View.prototype.render.apply(self, arguments);
			});
		}

	
	});
});
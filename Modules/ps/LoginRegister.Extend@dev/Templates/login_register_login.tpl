{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}




<form class="login-register-login-form" novalidate>
	<fieldset class="login-register-login-form-fieldset">
		<div class="login-register-login-form-controls-group" data-validation="control-group">
			<label class="login-register-login-form-label" for="login-email">
				{{translate 'User Name'}}
			</label>
			<div class="login-register-login-form-controls" data-validation="control">
				<input {{#if hasAutoFocus}} autofocus {{/if}} type="email" name="email" id="login-email" class="login-register-login-form-input"/>
			</div>
		</div>

		<div class="login-register-login-form-controls-group" data-validation="control-group">
			<label class="login-register-login-form-label" for="login-password">
				{{translate 'Password'}}
			</label>
			<div class="login-register-login-form-controls" data-validation="control">
				<input type="password" name="password" id="login-password" class="login-register-login-form-input">
			</div>
		</div>

		{{#if isRedirect}}
			<div class="login-register-login-form-controls-group" data-validation="control-group">
				<div class="login-register-login-form-controls" data-validation="control">
					<input value="true" type="hidden" name="redirect" id="redirect" >
				</div>
			</div>
		{{/if}}

		<div data-type="alert-placeholder" class="login-register-login-form-messages">
			{{#if isUserSessionTimedOut}}
				<div data-view="GlobalMessageSessionTimeout"></div>
			{{/if}}
		</div>

		<div class="login-register-login-form-controls-group" data-type="form-login-action">

			<button type="submit" class="login-register-login-submit" data-action="login-button">
				{{translate 'Log In'}}
			</button>

			<a class="login-register-login-forgot" data-action="forgot-password" href="/forgot-password">
				{{translate 'Forgot password?'}}
			</a>
		</div>

		<div class="login-register-bottom">
			<p>All personal information you submit is encrypted and secure.</p>	
			<div class="login-register-bottom-seal"></div>		
		</div>
	</fieldset>
</form>
	<!--Start Modal-->
	<div class="modal fade" id="modalTermsPassword" role="dialog">
    	<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Welcome to the skin<b>better</b> science&trade; Professional Portal.</h4>
				</div>
				<div class="modal-body">
					<p>
					Enrolling through this secure portal enables your practice to: (i) purchase skinbetter science products online; (ii) monitor order status; (iii) initiate your patient’s enrollment to provide them online access as a customer of skin<b>better</b> science; and (iv) track aggregate purchases for your account and the purchases across all customers for whom you have enrolled.
					</p>
					<p>
					We follow industry best practices to maintain the safety of your account information to ensure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed. Your account information is contained behind secured networks and all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. For more insight, you may also want to read our <a href="{{wsdklink}}privacy-policy/">Privacy Policy</a>. 
					</p>
					<p>
					By enrolling through this Portal, you acknowledge and consent that we may use the information that you provide us to supply you with educational, promotional, and other informational materials about our products, programs and services. Subsequent to this enrollment and opt­in, you may withdraw your consent at anytime from within your account or by e­mailing us at <a href="mailto:support@skinbetter.com">support@skinbetter.com</a>.</p>	
					</p>					 

					<p class="bold-text">Please Update Your Password</p>
					<div class="login-register-login-form-controls-group">
						<label class="login-register-login-form-label" for="login-password">
							{{translate 'Old Password <small class="login-register-login-form-required">*</small>'}}
						</label>
						<div class="login-register-login-form-controls">
							<input type="password" name="oldpassword" id="login-oldpassword" class="login-register-login-form-input">
						</div>
					</div>
					<div class="login-register-login-form-controls-group">
						<label class="login-register-login-form-label" for="login-password">
							{{translate 'New Password <small class="login-register-login-form-required">*</small>'}}
						</label>
						<div class="login-register-login-form-controls">
							<input type="password" name="newpassword" id="login-newpassword" class="login-register-login-form-input">
						</div>
					</div>
					<div class="login-register-login-form-controls-group">
						<label class="login-register-login-form-label" for="login-password">
							{{translate 'Re-enter Password <small class="login-register-login-form-required">*</small>'}}
						</label>
						<div class="login-register-login-form-controls">
							<input type="password" name="renewpassword" id="login-renewpassword" class="login-register-login-form-input">
						</div>
					</div>

					<label class="login-register-register-form-label">
						<input type="checkbox" name="termsandconditions" id="register-termsandconditions">
						{{translate 'I Agree to SkinBetter Science LLC Terms and Conditions'}}
					</label>
					<div data-type="alert-placeholder" id="login-register-tems-alert" class="login-register-login-form-messages">
 						<div class="global-views-message global-views-message-error alert" role="alert">asd</div> 
					</div>		
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="login-saveTermsPassword" data-action="login-updatedata">Submit</button>
				</div>
			</div>
      
		</div>
	</div>

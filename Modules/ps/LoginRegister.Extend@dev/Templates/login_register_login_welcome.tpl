{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<h2 class="login-register-login-title">{{translate 'SkinBetter Science Customer Portal'}}</h2>
<p class="login-register-login-description">{{translate 'Welcome to the SkinBetter Science Customer Portal'}}</p>

<div class="login-register-login-step-button-container">

	<button class="login-register-login-step-button-continue" data-action="submit-step">
		{{continueButtonLabel}}
	</button>		
</div>
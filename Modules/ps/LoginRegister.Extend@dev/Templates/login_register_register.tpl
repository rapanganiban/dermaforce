{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#unless showFormFieldsOnly}}
<h1 class="login-register-main-title">Welcome to the skinbetter science® authorized online store!</h1>
<p class="login-register-register-form-description">
	{{translate 'Please create an account to help us keep you connected to your skin care provider in our system. It will only take a moment, and once you create an account you can explore and shop for your favorite skinbetter science products anytime!'}}
</p>

<form class="login-register-register-form" method="POST" novalidate>
	<p class="login-register-register-required">{{translate 'Required <span class="login-register-register-form-required">*</span>'}}</p>

{{/unless}}

	<div class="login-register-register-form-controls-group" data-validation="control-group">
		<label class="login-register-register-form-label" for="register-firstname">
			{{translate 'First Name <small class="login-register-register-form-required">*</small>'}}
		</label>
		<div class="login-register-register-form-controls" data-validation="control">
			<input {{#if hasAutoFocus}} autofocus {{/if}} type="text" name="firstname" id="register-firstname" class="login-register-register-form-input">
		</div>
	</div>

	<div class="login-register-register-form-controls-group" data-validation="control-group">
		<label class="login-register-register-form-label" for="register-lastname">
			{{translate 'Last Name <small class="login-register-register-form-required">*</small>'}}
		</label>
		<div class="login-register-register-form-controls" data-validation="control">
			<input type="text" name="lastname" id="register-lastname" class="login-register-register-form-input">
		</div>
	</div>

	{{#if showCompanyField}}
		<div class="login-register-register-form-controls-group" data-validation="control-group">
			<label class="login-register-register-form-label" for="register-company">
				{{#if isCompanyFieldRequire}}
					{{translate 'Company <small class="login-register-register-form-required">*</small>'}}
				{{else}}
					{{translate 'Company'}} <small class="login-register-register-form-optional">{{translate '(optional)'}}</small>
				{{/if}}
			</label>
			<div class="login-register-register-form-controls" data-validation="control">
				<input type="text" name="company" id="register-company" class="login-register-register-form-input"/>
			</div>
		</div>
	{{/if}}

	<div class="login-register-register-form-controls-group" data-validation="control-group">
		<label class="login-register-register-form-label" for="register-email">
			{{translate 'Email Address <small class="login-register-register-form-required">*</small>'}}
		</label>
		<div class="login-register-register-form-controls" data-validation="control">
			<input type="email" name="email" id="register-email" class="login-register-register-form-input" placeholder="{{translate 'your@email.com'}}" value="{{customer_email}}">
			<p class="login-register-register-form-help-block">
				<!--small>
					{{translate 'We need your email address to contact you about your order.'}}
				</small-->
			</p>
		</div>
	</div>
	<div class="login-register-register-form-controls-group" data-validation="control-group">
		<label class="login-register-register-form-label" for="register-password">
			{{translate 'Password <small class="login-register-register-form-required">*</small>'}}
		</label>
		<div class="login-register-register-form-controls" data-validation="control">
			<input type="password" name="password" id="register-password" class="login-register-register-form-input">
		</div>
	</div>

	<div class="login-register-register-form-controls-group" data-validation="control-group">
		<label class="login-register-register-form-label" for="register-password2">
			{{translate 'Re-Enter Password <small class="login-register-register-form-required">*</small>'}}
		</label>
		<div class="login-register-register-form-controls" data-validation="control">
			<input type="password" name="password2" id="register-password2" class="login-register-register-form-input">
		</div>
	</div>

	{{#if hasB2bId}}
	<div class="login-register-register-form-controls-group" data-validation="control-group">
		<div class="login-register-register-form-controls" data-validation="control">
			<input type="hidden" name="b2bId" id="b2bId" value="{{b2bId}}">
		</div>
	</div>
	{{/if}}

	{{#if isRedirect}}
		<div class="login-register-register-form-controls-group" data-validation="control-group">
			<div class="login-register-register-form-controls" data-validation="control">
				<input value="true" type="hidden" name="redirect" id="redirect" >
			</div>
		</div>
	{{/if}}

	<div class="login-register-register-form-controls-group">
		<label class="login-register-register-form-label">
			<input type="checkbox" name="emailsubscribe" id="register-emailsubscribe" value="T" checked>
			{{translate 'Yes, I would like to be the first to hear about the latest brand news, exclusive offers and promotions &nbsp;'}}
		</label>
	</div>

	<div class="login-register-register-form-controls-group">
		<label class="login-register-register-form-label siteNameTerms">
			<input type="checkbox" name="termsandconditions" id="register-termsandconditions">
			{{translate 'I Agree to $(0)' siteName}}
		</label>
		<a class="terms-anchor" data-toggle="modal" data-target="#modalTerms">Terms and Conditions</a>
	</div>

	<div class="login-register-register-form-messages" data-type="alert-placeholder"></div>

{{#unless showFormFieldsOnly}}
	<div class="login-register-register-form-controls-group">
		<button type="submit" class="login-register-register-form-submit login-register-register-form-submit-margin">
			{{translate 'Create Account'}}
		</button>
	</div>
</form>
{{/unless}}

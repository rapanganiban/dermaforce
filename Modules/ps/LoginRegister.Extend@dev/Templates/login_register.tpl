{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<section class="login-register" >

	<header class="login-register-header">		
		<h1 class="login-register-title"> {{translate 'Log in'}}</h1>		
	</header>

	<div {{#if showRegister}} class="login-register-body" {{else}} class="login-register-body-colored" {{/if}}>

		{{#if showLogin}}
			<div class="login-register-wrapper-column-login">
				<div class="login-register-wrapper-login-skin" data-view="Login"></div>
			</div>
		{{/if}}		

	</div>
</section>
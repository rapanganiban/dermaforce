{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<h2 class="login-register-login-title">{{translate 'Returning customer'}}</h2>
<p class="login-register-login-description">{{translate 'Log in below to checkout with an existing account'}}</p>

<small class="login-register-login-required">{{translate 'Required <span class="login-register-login-form-required">*</span>'}}</small>

<form class="login-register-login-form" novalidate>
	<fieldset class="login-register-login-form-fieldset">
		<div class="login-register-login-form-controls-group" data-validation="control-group">
			<label class="login-register-login-form-label" for="login-email">
				{{translate 'Email Address <small class="login-register-login-form-required">*</small>'}}
			</label>
			<div class="login-register-login-form-controls" data-validation="control">
				<input {{#if hasAutoFocus}} autofocus {{/if}} type="email" name="email" id="login-email" class="login-register-login-form-input" placeholder="{{translate 'your@email.com'}}"/>
			</div>
		</div>

		<div class="login-register-login-form-controls-group" data-validation="control-group">
			<label class="login-register-login-form-label" for="login-password">
				{{translate 'Password <small class="login-register-login-form-required">*</small>'}}
			</label>
			<div class="login-register-login-form-controls" data-validation="control">
				<input type="password" name="password" id="login-password" class="login-register-login-form-input">
			</div>
		</div>

		{{#if isRedirect}}
			<div class="login-register-login-form-controls-group" data-validation="control-group">
				<div class="login-register-login-form-controls" data-validation="control">
					<input value="true" type="hidden" name="redirect" id="redirect" >
				</div>
			</div>
		{{/if}}

		<div data-type="alert-placeholder" class="login-register-login-form-messages">
			{{#if isUserSessionTimedOut}}
				<div data-view="GlobalMessageSessionTimeout"></div>
			{{/if}}
		</div>

		<div class="login-register-login-form-controls-group" data-type="form-login-action">

			<button type="submit" class="login-register-login-submit" data-action="login-button">
				{{translate 'Log In'}}
			</button>

			<a class="login-register-login-forgot" data-action="forgot-password" href="/forgot-password">
				{{translate 'Forgot password?'}}
			</a>
		</div>
	</fieldset>
</form>
	<!--Start Modal-->
	<div class="modal fade" id="modalTermsPassword" role="dialog">
    	<div class="modal-dialog">
		<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Terms and Conditions</h4>
				</div>
				<div class="modal-body">
					<p>Welcome to the skinbetter science™ Professional Login Portal. Enrolling your practice through this portal creates a secure account so you will be able to:<br>
					(i) purchase skinbetter products science online;<br>
					(ii) track orders to monitor shipping and delivery;<br> 
					(iii) initiate your patients’ enrollment to provide them online access as a customers of skinbetter science through their Personal Login Portal; and<br>
					(iv) track aggregate purchases for your account and total associated online purchases across all customers for whom you have initiated Personal Login Portal access. 
					<br><br>
					To protect your personal information, we follow industry best security practices to maintain the safety and integrity of your financial and other confidential information to make sure it is compromised, misused, or otherwise improperly accessed, disclosed or utilized. Additional information about privacy terms and how we use and secure the information you provide to us can be reviewed anytime by viewing our Privacy Policy available for you on the skinbetter.staging.wpengine.com website. 
					<br><br>
					By enrolling your account through this Portal, you acknowledge and consent that we use the information that you provide us to supply you with educational, promotional, and other informational materials about our products, programs, and services. Subsequent to this enrollment and opt-in, you may withdraw your consent at anytime by e-mailing us at (email – special e-mail by Bob before launch – i.e. <a href="mailto:optoutrequest@skinbetter.staging.wpengine.com">optoutrequest@skinbetter.staging.wpengine.com</a>).</p>	

					<p>Update your password</p>
					<div class="login-register-login-form-controls-group">
						<label class="login-register-login-form-label" for="login-password">
							{{translate 'Old Password <small class="login-register-login-form-required">*</small>'}}
						</label>
						<div class="login-register-login-form-controls">
							<input type="password" name="oldpassword" id="login-oldpassword" class="login-register-login-form-input">
						</div>
					</div>
					<div class="login-register-login-form-controls-group">
						<label class="login-register-login-form-label" for="login-password">
							{{translate 'New Password <small class="login-register-login-form-required">*</small>'}}
						</label>
						<div class="login-register-login-form-controls">
							<input type="password" name="newpassword" id="login-newpassword" class="login-register-login-form-input">
						</div>
					</div>
					<div class="login-register-login-form-controls-group">
						<label class="login-register-login-form-label" for="login-password">
							{{translate 'Re-enter Password <small class="login-register-login-form-required">*</small>'}}
						</label>
						<div class="login-register-login-form-controls">
							<input type="password" name="renewpassword" id="login-renewpassword" class="login-register-login-form-input">
						</div>
					</div>

					<label class="login-register-register-form-label">
						<input type="checkbox" name="termsandconditions" id="register-termsandconditions">
						{{translate 'I Agree to DermaForce Holdings LLC Terms and Conditions'}}
					</label>
					<div data-type="alert-placeholder" id="login-register-tems-alert" class="login-register-login-form-messages">
 						<div class="global-views-message global-views-message-error alert" role="alert">asd</div> 
					</div>		
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="login-saveTermsPassword" data-action="login-updatedata">Save</button>
				</div>
			</div>
      
		</div>
	</div>

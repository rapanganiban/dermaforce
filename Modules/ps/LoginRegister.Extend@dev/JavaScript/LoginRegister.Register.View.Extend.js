/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module LoginRegister
define('LoginRegister.Register.View.Extend'
,	[
		'LoginRegister.Register.View'
	,	'Account.Register.Model'	
	,	'Backbone.FormView'
	,	'SC.Configuration'
	,	'Backbone'
	,	'underscore'
	,	'Utils'
	]
,	function (
		LoginRegisterRegisterView
	,	AccountRegisterModel	
	,	BackboneFormView	
	,	Configuration
	,	Backbone
	,	_
	)
{
	'use strict';

	// @clasd LoginRegister.Register.View @extend Backbone.View
	return _.extend( LoginRegisterRegisterView.prototype, {

		
		initialize: function (options)
		{
			this.options = options;
			this.application = options.application;
			this.parentView = options.parentView;
			this.email = _.getParameterByName(this.el.baseURI, 'email');
            this.b2bId = _.getParameterByName(this.el.baseURI, 'invitecode');
			this.model = options.model || new AccountRegisterModel();
			// on save we redirect the user out of the registration page
			// as we know there hasn't been an error
			this.model.on('save', _.bind(this.redirect, this));

			BackboneFormView.add(this);
		}

		,	bindings: {
			'[name="firstname"]': 'firstname'
		,	'[name="lastname"]': 'lastname'
		,	'[name="company"]': 'company'		
		,	'[name="password"]': 'password'
		,	'[name="password2"]': 'password2'
		}

		,	getContext : _.wrap(LoginRegisterRegisterView.prototype.getContext, function(fn){
			var currentContext = fn.apply(this, _.toArray(arguments).slice(1));

			return _.extend(currentContext, {
				customer_email: (this.email || ""),
				b2bId: this.b2bId,
				hasB2bId: this.b2bId != "" && this.b2bId
			});
	    })

	,	redirect: function (context, response)
        {
            window.location.href = SC.ENVIRONMENT.siteSettings.touchpoints.home;
        }

	});
});
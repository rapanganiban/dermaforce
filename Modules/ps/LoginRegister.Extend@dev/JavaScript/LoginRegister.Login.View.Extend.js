/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module LoginRegister
define('LoginRegister.Login.View.Extend'
,	[	
		'LoginRegister.Login.View'

	,	'SC.Configuration'
	,	'Tracker'
	,	'Session'
	,	'Account.Login.Model'
	,	'LoginRegister.Utils'
	,	'Profile.Model'
	,	'LiveOrder.Model'

	,	'Backbone'
	,	'Backbone.FormView'
	,	'Backbone.CompositeView'
	,	'GlobalViews.Message.View'
	,	'underscore'
	,	'jQuery'
	,	'Utils'
	]
,	function (
		LoginRegisterLoginView

	,	Configuration
	,	Tracker
	,	Session
	,	AccountLoginModel
	,	LoginRegisterUtils
	,	ProfileModel
	,	LiveOrderModel

	,	Backbone
	,	BackboneFormView
	,	BackboneCompositeView
	,	GlobalViewsMessageView
	,	_
	,	jQuery
	)
{
	'use strict';

	// @class LoginRegister.Login.View Implements the login experience UI @extend Backbone.View
	return _.extend(LoginRegisterLoginView.prototype, {

		events: _.extend(LoginRegisterLoginView.prototype.events ,{
			'click [data-action="login-updatedata"]': 'updateCustomer'
		})

	,	render: _.wrap(LoginRegisterLoginView.prototype.render, function wrapRender(fn) {
            fn.apply(this, _.toArray(arguments).slice(1));
		
			 this.showNortonImage();
		})	

	,	showNortonImage: function(){

		/*var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'https://seal.websecurity.norton.com/getseal?host_name=store.skinbetter.com&size=M&use_flash=NO&use_transparent=YES&lang=en'
        document.getElementsByClassName("login-register-bottom")[0].appendChild(script);
*/
		//this.once('afterAppendView', function(){
			/*jQuery('<script/>', {
				type: 'text/javascript',
				src: 'https://seal.websecurity.norton.com/getseal?host_name=store.skinbetter.com&size=M&use_flash=NO&use_transparent=YES&lang=en'
			}).appendTo(jQuery(this.$el).find(".login-register-bottom"));*/
		//})
		jQuery(this.$el).find(".login-register-bottom-seal").html(jQuery("#sealtable").find("td").html())
	}

	,	updateCustomer : function(){

			jQuery("#login-register-tems-alert").html("");
			var self = this
			,	message = null;
			this.model.set('oldPassword' , jQuery('#login-oldpassword').val())
			this.model.set('newPassword' , jQuery('#login-newpassword').val())
			this.model.set('termsAccepted' , "T");

			if(jQuery('#login-oldpassword').val() ==""){
				message ='<div class="global-views-message global-views-message-error alert" role="alert">Please enter value for old Password</div>' 
			}else if (jQuery('#login-newPassword').val() ==""){
				message ='<div class="global-views-message global-views-message-error alert" role="alert">Please enter value for new Password</div>' 
			}else if (jQuery('#login-renewpassword').val() != jQuery('#login-newpassword').val()){
				message ='<div class="global-views-message global-views-message-error alert" role="alert">The new password do not match</div>' 
			}else if (jQuery('#login-oldpassword').val() == jQuery('#login-newpassword').val()){
				message ='<div class="global-views-message global-views-message-error alert" role="alert">The new password must be different from old password</div>' 
			}else if(jQuery("#register-termsandconditions:checked").length <= 0){
				message ='<div class="global-views-message global-views-message-error alert" role="alert">Terms and Conditions are required</div>' 
			}
			
			if(message){
				jQuery("#login-register-tems-alert").html(message);
			}else{
				this.model.save().done(function(){
					self.redirectCustomer(self.response);
				}).error(function(error){
					message ='<div class="global-views-message global-views-message-error alert" role="alert">'+error.errorMessage+'</div>' 
					jQuery("#login-register-tems-alert").html(message);
				});
			}
		}

		// @method redirect Redirects the user after successful login taking into account redirect parameters (origin and origin_hash).
		// @param {Object} response The HTTP response data object, response of the login service.
	,	redirect: function (context, response)
		{		
			var self = this;
			this.model.fetch().done(function(responseFetch){	

				if(responseFetch.terms == "F"){
					self.response = response;
					jQuery('#modalTermsPassword').modal({
					    backdrop: 'static',
					    keyboard: false
					});					
				}else{			
					self.redirectCustomer(response)
				}
			})//end fetch
		}//end redirect

	,	redirectCustomer: function(response){
			var url_options = _.parseUrlOptions(window.location.search)
			,	touchpoints = response.touchpoints
			,	isPasswordReset = url_options.passwdret
			,	self = this;

			// Track Login Event
			this.trackEvent(function ()
			{
				if (!isPasswordReset && (url_options.is === 'checkout' || url_options.origin === 'checkout'))
				{
					self.refreshApplication(response);
				}
				else
				{					
					// if we know from which touchpoint the user is coming from
					if (url_options.origin)
					{						
						var url = url_options.origin + "?ck="+ touchpoints.welcome.split("ck=")[1]

						if(touchpoints[url_options.origin]){
							// we save the URL to that touchpoint
							url = touchpoints[url_options.origin];
							// if there is an specific hash
							if (url_options.origin_hash)
							{
								// we add it to the URL as a fragment
								url = _.addParamsToUrl(url, {fragment: url_options.origin_hash});
							}
						}

						window.location.href = url;
					}
					else
					{
						//We've got to disable passwordProtectedSite and loginToSeePrices features if customer registration is disabled.
						if(Configuration.getRegistrationType() !== 'disabled' && SC.getSessionInfo('passwordProtectedSite'))
						{
							window.location.href = touchpoints.home;
						}
						else
						{
						// otherwise we need to take it to the customer center
						window.location.href = touchpoints.customercenter;
						}
					}
				}
			})//end trackEvent
		}
	//@method getContext @return {LoginRegister.Login.View.Context}
	,	getContext: function ()
		{
			var url_options = _.parseUrlOptions(window.location.search);

			//@class LoginRegister.Login.View.Context
			return {
				// @property {Boolean} isRedirect
				isRedirect: !!(url_options.is !== 'checkout' && url_options.origin !== 'checkout')
				// @property {Boolean} hasAutoFocus
			,	hasAutoFocus: url_options.is !== 'register' && _.isDesktopDevice()
				// @property {Boolean} isUserSessionTimedOut
			,	isUserSessionTimedOut: url_options.timedout === 'T' || this.timedout
			,	nortonImgLogo: _.getAbsoluteUrl('img/norton.jpg')
			,	wsdklink: SC.ENVIRONMENT.siteSettings.wsdkcompleteloginurl
			};
		}
	});
});
/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module LoginRegister
define('LoginRegister.Router.Extend'
,	[
		'LoginRegister.Router'
	,	'LoginRegister.Register.View'
	,	'Backbone'
	,	'underscore'
	]
,	function (
		LoginRegisterRouter
	,	RegisterView

	,	Backbone
	,	_
	)
{
	'use strict';

	// @class LoginRegister.Router Handles views and routers of Login/Register Page. Includes Register Guest, Forgot Password and Reset password.
	// Initializes the different views depending on the requested path.  @extend Backbone.Router
	return _.extend(LoginRegisterRouter.prototype, {

		routes: _.extend(LoginRegisterRouter.prototype.routes, {
				'register?option': 'register'		
			})

	,	register: function (option)
		{
			if (this.profileModel.get('isLoggedIn') === 'T' && this.profileModel.get('isGuest') === 'F')
			{
				Backbone.history.navigate('', { trigger: true });
			}
			else
			{
				var view = new RegisterView({
					application: this.application,
					email : option
				});
				view.title = _('Register').translate();
				view.showContent();
			}
		}	
	});
});
//Model.js
// PlacedOrder.js
// ----------
// Handles fetching orders

/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

define('AgreggatedFullList.Model',
	[
    	'SC.Model'
   	,	'underscore'
	]
, function AgreggatedFullListModel(
    SCModel,
    _
) {
    'use strict';

    return SCModel.extend({

        name: 'AgreggatedFullList'

	,	list: function (data)
		{
			data = data || {};

			_.extend(data,{
				cid : nlapiGetUser()
			})

			var url = nlapiResolveURL('SUITELET', 'customscript_fullproductlist_filters', 'customdeploy_fullproductlist_filters', true)
			,	response = nlapiRequestURL(url, data)
			,	result   = {};


			if(response.getBody())
			{
				result = JSON.parse( response.getBody() );
			}

			return result;
		}

    });
    
});
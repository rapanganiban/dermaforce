//Model.js
// PlacedOrder.js
// ----------
// Handles fetching orders

/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
*/

define('AgreggatedTotal.Model',
	[
    	'SC.Model'
   	,	'underscore'
	]
, function AgreggatedTotalModel(
    SCModel,
    _
) {
    'use strict';

    return SCModel.extend({

        name: 'AgreggatedTotal'

	,	list: function (data)
		{
			data = data || {};

			_.extend(data,{				
				cid : nlapiGetUser()
			})			
			
			var url = nlapiResolveURL('SUITELET', 'customscript_agreggatedtotal_management', 'customdeploy_agreggatedtotal_management', true)
			,	response = nlapiRequestURL(url, data)
			,	result   = {};

			
			if(response.getBody())
			{
				result = JSON.parse( response.getBody() );						
			}

			return result;
		}

	,	download: function (data)
		{
			data = data || {};

			_.extend(data,{				
				cid : nlapiGetUser()
			})			
			
			var url = nlapiResolveURL('SUITELET', 'customscript_agreggatedtotal_management', 'customdeploy_agreggatedtotal_management', true)
			,	response = nlapiRequestURL(url, data)
			,	result   = {};

			
			if(response.getBody())
			{
				result = JSON.parse( response.getBody() );						
			}


			var fileContent = "SKU,Patient Name,Product Name,Transcation #,Date,Quantity,Amount";

			var contents = JSON.parse(JSON.stringify(result));

			var totalAmt = contents.total;
			var totalQty = contents.quantitytotal;

			var rows = contents.records;
			var lines = '';

			rows.forEach(function(record) {
				var patientName = record.firstname + ' ' + record.lastname;
				lines = '\n' + record.sku + ',' + patientName + ',' + record.name + ',' + record.salesorder + ',' + record.date + ',' + record.quantity + ',' + record.amount;
				fileContent += lines;
			})


			return fileContent;
		}

    });
    
});
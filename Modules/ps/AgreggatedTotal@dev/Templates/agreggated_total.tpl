{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showBackToAccount}}
	<a href="/" class="order-history-list-button-back">
		<i class="order-history-list-button-back-icon"></i>
		{{translate 'Back to Account'}}
	</a>
{{/if}}

<section class="order-history-list">
	<header class="order-history-list-header">
		<h2>{{pageHeader}}</h2>
	</header>

	{{#if showStatusFilter}}
		<div class="order-history-list-open-list-header-nav">
			<div class="order-history-list-open-list-header-button-group">
				<span class="order-history-list-open-list-header-button-open">{{translate 'Open'}}</span>
				<a href="/purchasesAll" class="order-history-list-open-list-header-button-all">{{translate 'All'}}</a>
			</div>
		</div>
	{{/if}}

	<div data-view="ListHeader.AgreggatedTotal"></div>

	{{#if collectionLengthGreaterThan0}}

	<div class="order-history-list-recordviews-container">
		<table class="order-history-list-recordviews-actionable-table">
			<thead class="order-history-list-recordviews-actionable-header">
				<tr>					
					<th class="order-history-list-recordviews-actionable-title-header">
						<span>{{translate 'SKU'}}</span>
					</th>
					<th class="order-history-list-recordviews-actionable-title-header">
						<span>{{translate 'Patient Name'}}</span>
					</th>
					<th class="order-history-list-recordviews-actionable-title-header">
						<span>{{translate 'Product Name'}}</span>
					</th>
					<th class="order-history-list-recordviews-actionable-title-header">
						<span>{{translate 'Transaction #'}}</span>
					</th>
					<th class="order-history-list-recordviews-actionable-title-header">
						<span>{{translate 'Date'}}</span>
					</th>
					<th class="order-history-list-recordviews-actionable-title-header">
						<span>{{translate 'Quantity'}}</span>
					</th>
					<th class="order-history-list-recordviews-actionable-title-header">
						<span>{{translate 'Total'}}</span>
					</th>					
				</tr>
			</thead>
			<tbody class="order-history-list" data-view="AgreggatedTotal.Result"></tbody>
		</table>
	</div>

	<div class="text-right" style="text-align=right;">Total Spend: ${{total}}</div>
	<div class="text-right" style="text-align=right;">Total Quantity: {{quantitytotal}}</div>
	
	{{else}}
		{{#if isLoading}}
			<p class="order-history-list-empty">{{translate 'Loading...'}}</p>
		{{else}}
			<div class="order-history-list-empty-section">
				<h5>{{translate 'No Purchase History To Report'}}</h5>
			</div>
		{{/if}}

	{{/if}}

	{{#if showPagination}}
		<div class="order-history-list-case-list-paginator">
			<div data-view="GlobalViews.Pagination"></div>
			{{#if showCurrentPage}}
				<div data-view="GlobalViews.ShowCurrentPage"></div>
			{{/if}}
		</div>
	{{/if}}
</section>
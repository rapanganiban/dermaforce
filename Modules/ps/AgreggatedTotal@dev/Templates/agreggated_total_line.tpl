{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<tr class="recordviews-row">
	<td class="recordviews-label">
		<span>{{sku}}</span>
	</td>
	<td class="recordviews-label">
		<span>{{patientName}}</span>
	</td>
	<td class="recordviews-label">
		<span>{{name}}</span>
	</td>
	<td class="recordviews-label">
		<span>{{salesorder}}</span>
	</td>
	<td class="recordviews-label">
		<span>{{date}}</span>
	</td>	
	<td class="recordviews-label">
		<span>{{quantity}}</span>
	</td>
	<td class="recordviews-label">
		<span>${{total}}</span>
	</td>	
</tr>
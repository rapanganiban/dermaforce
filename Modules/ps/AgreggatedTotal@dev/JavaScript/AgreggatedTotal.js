// AgreggatedTotal.js
// -----------------
// Defines the AgreggatedTotal  module (Model, Collection, View, Router)
define(
	'AgreggatedTotal'
,	[
		'AgreggatedTotal.View'
	,	'AgreggatedTotal.Router'	
	,	'underscore'
	, 	'Utils'
	]
,	function (View,  Router)
	{
		'use strict';

				
		return	{
			View: View
			
		,	Router: Router
		
		,	MenuItems:  {
				id: 'reporting'
			,	name: _('Reports').translate()
			,	index: 4
			,	children: [{	
						id: 'reportingtotal'
					,	name: _('Patient Sales by Item').translate()
					,	url: 'reportingtotal'
					,	index: 2
					}]			
					} 
		
		,	mountToApp: function (application)
			{
				return new Router(application);
			}
		};
	}
);			
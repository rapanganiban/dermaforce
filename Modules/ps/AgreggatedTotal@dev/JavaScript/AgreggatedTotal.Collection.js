// AgreggatedTotal.Collection.js
// -----------------------
// AgreggatedTotal collection
define('AgreggatedTotal.Collection'
,	[
		'AgreggatedTotal.Model'
	,	'Backbone'
	,	'underscore'
	,	'Utils'
	]
, function (
		Model
	,	Backbone
	,	_
)
{
	'use strict';
	
	return Backbone.Collection.extend(
	{
		model: Model

	,	url: _.getAbsoluteUrl('services/AgreggatedTotal.Service.ss')

	,	parse: function (response)
		{
			this.totalRecordsFound = response.totalRecordsFound;
			this.recordsPerPage = response.recordsPerPage;
			this.quantitytotal = response.quantitytotal;
			this.total = response.total;
			return response.records;
		}

	,	update: function (options)
		{
			var range = options.range || {};

			this.fetch({
				data: {
					filter: options.filter && options.filter.value
				,	sort: options.sort.value
				,	order: options.order
				,	from: range.from ? new Date(range.from.replace(/-/g,'/')).getTime() : null
				,	to: range.to ? new Date(range.to.replace(/-/g,'/')).getTime() : null
				,	page: options.page
				}
			,	reset: true
			,	killerId: options.killerId
			});
		}

	});
});

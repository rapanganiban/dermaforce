// AgreggatedTotal.Model.js
// -----------------------
define('AgreggatedTotal.Model', ['Backbone', 'underscore', 'Utils'], function ( Backbone, _ )
{
	'use strict';
	
	return Backbone.Model.extend(
	{
		urlRoot: _.getAbsoluteUrl('services/AgreggatedTotal.Service.ss')
	});
});

/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderHistory
define('AgreggatedTotal.View'
,	[	'AgreggatedTotal.Model'
	,	'SC.Configuration'
	,	'GlobalViews.Pagination.View'
	,	'GlobalViews.ShowingCurrent.View'
	,	'ListHeader.AgreggatedTotal.View'
	,	'Backbone.CompositeView'
	,	'Backbone.CollectionView'
	,	'AgreggatedTotal.Line.View'
	,	'Handlebars'

	,	'agreggated_total.tpl'

	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	]
,	function (
		AgreggatedTotalModel
	,	Configuration
	,	GlobalViewsPaginationView
	,	GlobalViewsShowingCurrentView
	,	ListHeaderAgreggatedTotalView
	,	BackboneCompositeView
	,	BackboneCollectionView
	,	AgreggatedTotalLineView
	,	Handlebars

	,	agreggated_total_tpl

	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';

	//@class OrderHistory.List.View view list of orders @extend Backbone.View
	return  Backbone.View.extend({
		//@property {Function} template
		template: agreggated_total_tpl
		//@property {String} title
	,	title: _('Patient Sales by Item').translate()
		//@property {String} className
	,	className: 'OrderListView'
		//@property {String} page_header
	,	page_header: _('Patient Sales by Item').translate()
		//@property {Object} attributes
	,	attributes: {
			'class': 'OrderListView'
		}		

		//@method getSelectedMenu
	,	getSelectedMenu: function ()
		{
			return 'reportingtotal';
		}
		//@method getBreadcrumbPages
	,	getBreadcrumbPages: function ()
		{
			return {
					text: this.title
				,	href: '/agreggatedtotal'
			};
		}
		//@method initialize
	,	initialize: function (options)
		{
			this.application = options.application;
			this.collection = options.collection;
		
			var isoDate = _.dateToString(new Date());

			this.rangeFilterOptions = {
				fromMin: '1800-01-02'
			,	fromMax: isoDate
			,	toMin: '1800-01-02'
			,	toMax: isoDate
			};

			this.listenCollection();

			// Manages sorting and filtering of the collection
			this.listHeader = new ListHeaderAgreggatedTotalView({
				view: this
			,	application: this.application
			,	collection: this.collection
			,	sorts: this.sortOptions
			,	filters : options.filters
			,	rangeFilter: 'date'
			,	rangeFilterLabel: _('From').translate()
			,	hidePagination: true
			});

			this.showStatusFilter = options.showStatusFilter;

			BackboneCompositeView.add(this);
		}
		
		//@method listenCollection
	,	listenCollection: function ()
		{
			this.setLoading(true);

			this.collection.on({
				request: jQuery.proxy(this, 'setLoading', true)
			,	reset: jQuery.proxy(this, 'setLoading', false)
			});
		}
		//@method setLoading
	,	setLoading: function (value)
		{
			this.isLoading = value;
		}
		//@property {Array} sortOptions
		// Array of default sort options
		// sorts only apply on the current collection
		// which might be a filtered version of the original
	,	sortOptions: [
			{
				value: 'amount'
			,	name: _('Sort By Total').translate()
			,	selected: true
			}
		,	{
				value: 'itemid'
			,	name: _('Sort By SKU').translate()
			}
		]
		//@property {Object} childViews
	,	childViews: {
			'ListHeader.AgreggatedTotal': function ()
			{	
				return this.listHeader;
			}
		,	'GlobalViews.Pagination': function ()
			{
				return new GlobalViewsPaginationView(_.extend({
					totalPages: Math.ceil(this.collection.totalRecordsFound / this.collection.recordsPerPage)
				}, Configuration.defaultPaginationSettings));
			}
		,	'GlobalViews.ShowCurrentPage': function ()
			{
				return new GlobalViewsShowingCurrentView({
					items_per_page: this.collection.recordsPerPage
		 		,	total_items: this.collection.totalRecordsFound
				,	total_pages: Math.ceil(this.collection.totalRecordsFound / this.collection.recordsPerPage)
				});
			}
		,	'AgreggatedTotal.Result': function ()
			{
				var self = this
				,	records_collection = new Backbone.Collection(this.collection.map(function (product)
					{						
						var columns = [
							{
								label: _('Name:').translate()
							,	type: 'text'
							,	name: 'name'
							,	value: product.get('name')
							}
						,	{						
								label: _('Quantity:').translate()
							,	type: 'number'
							,	name: 'quantity'
							,	value: product.get('quantity')
							}
						,
						,	{
								label: _('Total: ($)').translate()
							,	type: 'text'
							,	name: 'total'
							,	value: product.get('total')
							}						
						];

						var model = new Backbone.Model({
								//@property {String} name
								name: product.get('name')
								//@property {int} quantity
							,	quantity: product.get('quantity')
								// @property {int} total
							,	total: product.get('amount')
								// @property {String} description
							,	sku: product.get('sku')	
								// @property {Date} description
							,	date: product.get('date')	
								// @property {String} description
							,	salesorder: product.get('salesorder')
							,	patientName: product.get('firstname') != '' && product.get('lastname') != '' ? product.get('firstname') + ' ' + product.get('lastname') : ' ' 	

							,	columns: columns
							});

						return model;
					}));

				return new BackboneCollectionView({
					childView: AgreggatedTotalLineView
				,	collection: records_collection
				,	viewsPerRow: 1
				});
			}
		}

		//@method getContext @return OrderHistory.List.View.Context
	,	getContext: function ()
		{
			//@class OrderHistory.List.View.Context
			return {
				//@property {String} pageHeader
				pageHeader: this.page_header
				//@property {Boolean} collectionLengthGreaterThan0
			,	collectionLengthGreaterThan0: this.collection.length > 0
				//@property {Boolean} isLoading
			,	isLoading: this.isLoading
				// @property {Boolean} showPagination
			,	showPagination: !!(this.collection.totalRecordsFound && this.collection.recordsPerPage)
				// @property {Boolean} showCurrentPage
			,	showCurrentPage: this.options.showCurrentPage
				//@property {Boolean} showBackToAccount
			,	showBackToAccount: Configuration.get('siteSettings.sitetype') === 'STANDARD'
				//@property {Boolean} showStatusFilter
			,	showStatusFilter: this.showStatusFilter || false
				//@property {Integer} total
			,	total : parseFloat(this.collection.total).toFixed(2)
				//@property {Integer} total
			,	quantitytotal : this.collection.quantitytotal		
			};
		}
	});

});
// AgreggatedTotal.Download.Model.js
// -----------------------
define('AgreggatedTotal.Download.Model', ['Backbone'], function ( Backbone )
{
	'use strict';
	
	return Backbone.Model.extend(
	{
		urlRoot: '/checkout/services/AgreggatedTotal.Download.Service.ss'
	});
});

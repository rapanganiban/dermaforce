// AgreggatedFullList.Model.js
// -----------------------
define('AgreggatedFullList.Model', ['Backbone', 'underscore', 'Utils'], function ( Backbone, _ )
{
	'use strict';
	
	return Backbone.Model.extend(
	{
		urlRoot: _.getAbsoluteUrl('services/AgreggatedTotal.FullList.Service.ss')
	});
});

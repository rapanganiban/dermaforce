// AgreggatedTotal.Router.js
// -----------------------
define(
	'AgreggatedTotal.Router'
,	[
		'AgreggatedTotal.View'
	,	'AgreggatedTotal.Collection'
	,	'AgreggatedTotal.Model'
	,	'AgreggatedFullList.Model'
	,	'Backbone'
	]
,	function (
		View
	,	Collection
	,	Model
	,	AgreggatedFullListModel
	,	Backbone
	)
{
		'use strict';

		return Backbone.Router.extend({

			routes: {
				'reportingtotal': 'agreggatedtotal'
			}

		,	initialize: function (application)
			{
				this.application = application;
			}

			// list download History
		,	agreggatedtotal: function (options)
			{
				options = (options) ? SC.Utils.parseUrlOptions(options) : {page: 1};

				options.page = options.page || 1;

				var model = new AgreggatedFullListModel()
				self = this;

				model.fetch().done(function(data){

					var filters = _.map(data.records, function(record){
					 return {
					 			value: record.sku
					 		,	name: record.sku+", "+record.name
					 		}
					});
					filters = _.uniq(filters, function(record){
					 return record.value
					});
					filters = _.union([{value: "",	name: "Filter by Item",	selected: true}], filters)
					var collection = new Collection()
					,	view = new View({
							application: self.application
						,	page: options.page
						,	collection: collection
						,	filters: filters
						});

					collection.on('reset', view.showContent, view);

					view.showContent();
				});
			}

			// full list of products
		,	agreggatedtotal1: function (options)
			{
				options = (options) ? SC.Utils.parseUrlOptions(options) : {page: 1};

				options.page = options.page || 1;

				var model = new Model()
				self = this;

				model.fetch().done(function(data){

					var filters = _.map(data.records, function(record){
					 return {
					 			value: record.sku
					 		,	name: record.sku+", "+record.name
					 		}
					});
					filters = _.uniq(filters, function(record){
					 return record.value
					});
					filters = _.union([{value: "",	name: "Filter by Item",	selected: true}], filters)
					var collection = new Collection()
					,	view = new View({
							application: self.application
						,	page: options.page
						,	collection: collection
						,	filters: filters
						});

					collection.on('reset', view.showContent, view);

					view.showContent();
				});
			}
		});
	}
);

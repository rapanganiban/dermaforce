function service(request, response) {
    var method = 'retrieveProductPurchase';

    switch (method) {
        case 'retrieveProductPurchase':
        default:
            retrieveProductPurchase(request, response);
    }
}

/**
 * retrieve the so
 */
function retrieveProductPurchase(request, response) {

    var objResponse = {
        page: page
        ,   recordsPerPage: results_per_page
        ,   records: []
    };;
    var cid    = nlapiGetUser() > 0? nlapiGetUser() : request.getParameter('cid');
    var paramenters = request.getAllParameters();

    try {

        var filters =   [
                new nlobjSearchFilter("custentity_sc_invite_sent","customer","is",cid)
            ],
            columns = [],
            total = 0.00,
            quantitytotal = 0;

        if( paramenters.from && paramenters.to )
        {
            var offset = new Date().getTimezoneOffset() * 60 * 1000;

            filters = _.union(filters, [
                new nlobjSearchFilter('trandate', null, 'within', new Date( parseInt( paramenters.from, 10 ) + offset ), new Date( parseInt( paramenters.to, 10 ) + offset ) )
            ]);
        }

        if(paramenters.filter && paramenters.filter != ""){
            paramenters.all = false;
            filters.push( new nlobjSearchFilter("itemid","item","is",paramenters.filter))
        }


        var transactions = nlapiSearchRecord(
            'transaction'
            ,   'customsearch_sc_sales_by_product'
            ,   filters
            ,   columns
        );

        if(transactions && transactions.length )
        {
            objResponse.totalRecordsFound = transactions.length;

            _.each(transactions, function(so){
                total += parseFloat(so.getValue("amount"))
                quantitytotal += parseInt(so.getValue("quantity"))
            })

            objResponse.total = total % 1 == 0 ? total+'.00' : total;

            objResponse.quantitytotal = quantitytotal;

            var results_per_page = 20
                ,   page = paramenters.page || 1
                ,   range_start = (page * results_per_page) - results_per_page
                ,   range_end = page * results_per_page;

            objResponse.recordsPerPage = results_per_page;
            objResponse.page = page;

            var search = nlapiLoadSearch('transaction', 'customsearch_sc_sales_by_product');

            var new_columns = []

            if( paramenters.sort){

                new_columns = _.map(search.columns, function(column){

                    if(paramenters.sort == column.getName())
                        column.setSort(( paramenters.order == -1 ))
                    else
                        column.sortdir = null;

                    return column;
                });
            }


            _.each(filters, function(filter){
                search.addFilter(filter)
            })
            var new_search = nlapiCreateSearch('transaction', search.getFilters(), new_columns);

            new_search = new_search.runSearch();

            if (paramenters.all) {
                transactions = new_search.getResults(0,1000);
            }
            else
            {
                transactions = new_search.getResults(range_start, range_end);
            }

            objResponse.records =   _.map(transactions, function(so)
            {
                return {
                    quantity : so.getValue("quantity"),
                    amount: so.getValue("amount"),
                    date: so.getValue("trandate"),
                    salesorder: so.getValue("formulatext"),
                    name: so.getValue("storedisplayname","item"),
                    sku: so.getValue("itemid","item"),
                    firstname: so.getValue("firstname", "customer"),
                    lastname: so.getValue("lastname", "customer")
                };
            });
        }

    } catch (e) {
        nlapiLogExecution('error', 'error', e);
    }

    response.setContentType('json');
    response.write(JSON.stringify(objResponse));
}
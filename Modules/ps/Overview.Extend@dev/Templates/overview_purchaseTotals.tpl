{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<section class="overview-home-header">
	<div class="row">
		<div class="col-md-8 col-sm-12">
			<h1 class="overview-title-header">Overview</h1>
			<h2>Welcome {{customerName}}</h2>
		</div>
		{{#if showSalesRep}}
		<!--<div class="col-md-4 col-sm-12 overview-salesRep">
			<p>Your Aesthetic Business Mgr.</p>
			<p>{{salesRepName}}</p>
			<p>{{salesRepEmail}}</p>
			<p>{{salesRepPhone}}</p>
		</div>-->
		{{/if}}
	</div>
	<div class="row icons-header">
		<div class="col-md-4 col-xs-12"><a href="#" class="icons-header-a" data-touchpoint="customercenter" data-hashtag="#/quickorder">New Order</a></div>
		<div class="col-md-4 col-xs-12"><a href="#" class="icons-header-a" data-touchpoint="customercenter" data-hashtag="#/reorderItems">Reorder</a></div>
		<div class="col-md-4 col-xs-12"><a href="#" class="icons-header-a" data-touchpoint="customercenter" data-hashtag="#/customerinvite">Enroll Patient</a></div>
	</div>
</section>

<section class="overview-home-mysettings">
	<h3>-- {{translate 'YTD Sales History'}} --</h3>
	<div class="overview-home-mysettings-row">
		<div class="col-md-6 col-sm-12">
			<div class="overrview-home-order-name">{{customerName}}</div>
			<div class="col-xs-6">
				<p>Orders</p>
				<p class="results-orders">
					{{totalSpend}}
				</p>
			</div>
			<div class="col-xs-6">
				<p>Total Spend:</p>
				<p class="results-orders">
					$ {{customersOrders}}
				</p>
			</div>
			<div class="overview-home-orders-by-date">
				<canvas id="canvasChart"></canvas>
			</div>
		</div>
		<div class="col-md-6 col-sm-12">
			<div class="overrview-home-order-name">Enrolled Patient</div>
			<div class="overview-order-totals">
				<div class="col-xs-6">
					<p>Orders</p>
					<p class="results-orders">
						{{enrolledCustomerTotal}}
					</p>
				</div>
				<div class="col-xs-6">
					<p>Total Spend:</p>
					<p class="results-orders">
						$ {{enrolledCustomerOrder}}
					</p>
				</div>
			</div>
			<div class="overview-home-orders-by-date">
				<canvas id="canvasChart2"></canvas>
			</div>
		</div>
	</div>
</section>
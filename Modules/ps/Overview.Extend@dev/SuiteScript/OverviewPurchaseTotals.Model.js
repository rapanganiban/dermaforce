/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// OverviewPurchaseTotals.Model.js
// --------------
// Defines the model used by the CustomerInvite.Service.ss service
define(
	'OverviewPurchaseTotals.Model'
,	[
		'SC.Model'
	,	'Utils'
	,	'Application'
	
	]
,	function (

		SCModel
	,	Utils
	,	Application
	
	)
{
	'use strict';

	// @class OverviewPurchaseTotals.Model Defines the model used by the OverviewPurchaseTotals.Service.ss service
	// @extends SCModel
	return SCModel.extend({

		//@property {String} name
		name: 'OverviewPurchaseTotals'

		//@method get
		//@param {String} email
		//@return {boolean}
	,	get: function (email)
		{	
			var customerid = nlapiGetUser()
			,	lineCasesServiceURL = 	nlapiResolveURL(
											'SUITELET',
											'customscript_sc_get_customer_totals',
											'customdeploy_sc_get_customer_totals',
											true
										) + '&customerid=' + customerid + "&method=getData"							
			,	lineCasesResponse   =	nlapiRequestURL( lineCasesServiceURL )
			,	lineCases = JSON.parse(lineCasesResponse ? lineCasesResponse.getBody() : '{}');

			return lineCases;
		}		
	});
});
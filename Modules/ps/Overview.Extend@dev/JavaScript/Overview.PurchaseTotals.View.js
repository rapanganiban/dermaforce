/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// Overview.Home.View.js
// --------------------

define('Overview.PurchaseTotals.View'
,	[
		'SC.Configuration'

	,	'overview_purchaseTotals.tpl'
	,	'Overview.PurchaseTotals.Model'

	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'underscore'
	,	'jQuery'
	,	'Utils'
	,	'ChartJS'
	]
,	function(
		Configuration

	,	overview_profile_tpl

	,	overviewPurchasetotalsModel

	,	Backbone
	,	BackboneCompositeView
	,	_
	,	jQuery
	,	Utils
	)
{
	'use strict';

	return Backbone.View.extend({

		template: overview_profile_tpl

	,	initialize: function (options)
		{
			BackboneCompositeView.add(this);	
			this.customerData = {}
			var self = this
			,	model = new overviewPurchasetotalsModel() 
			model.fetch().done(function( data ) {
			   self.customerData = {
			   		totalSpend : data.total,
			   		customersOrders: data.orders,
			   		enrolledCustomerTotal : data.consumersTotals,
			   		enrolledCustomerOrder : data.consumersOrders,
			   		ordersByDate : data.ordersByDate,
			   		consumersOrdersByDate : data.consumersOrdersByDate,
			   		salesrep : data.salesrep
			   	}
			   try{	
				   	self.render();

					var barChartData = {
					  labels: _.map(_.toArray(data.consumersOrdersByDate), function(date){ return date.month;}),
					  datasets: [{
					    fillColor: "rgba(0,60,100,1)",
					    strokeColor: "black",
					    data: _.map(_.toArray(data.consumersOrdersByDate), function(date){ return date.value;})
					  }]
					}

					var ctx = document.getElementById("canvasChart2").getContext("2d");
					var options = {
					  responsive: true,
					  barValueSpacing: 2,
					  scaleLabel : "<%= '$' + value  %>",
					  // String - Template string for single tooltips
					  tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= '$' + value %>",				 
					};
					var barChartDemo = new Chart(ctx).Bar(barChartData,options);

					var barChartData2 = {
					  labels: _.map(_.toArray(data.ordersByDate), function(date){ return date.month;}),
					  datasets: [{
					    fillColor: "rgba(0,60,100,1)",
					    strokeColor: "black",
					    data: _.map(_.toArray(data.ordersByDate), function(date){ return date.value;})
					  }]
					}

					var ctx2 = document.getElementById("canvasChart").getContext("2d");
					var barChartDemo = new Chart(ctx2).Bar(barChartData2, options);
				}catch(e){}
			})				
		}


	,	getContext : function()
		{			
		      //@class Overview.Banner.View.Context
			return {
				//@property {String} customersOrders
				customersOrders: this.customerData.customersOrders ||0
				//@property {String} totalSpend
			,	totalSpend:  this.customerData.totalSpend || 0
				//@property {String} enrolledCustomerTotal
			,	enrolledCustomerTotal: this.customerData.enrolledCustomerTotal || 0
				//@property {String} enrolledCustomerOrder
			,	enrolledCustomerOrder: this.customerData.enrolledCustomerOrder || 0
				//@property {String} ordersByDate
			,	ordersByDate: this.customerData.ordersByDate || {}
				//@property {String} consumersOrdersByDate
			,	consumersOrdersByDate: this.customerData.consumersOrdersByDate || {}
				//@property {String} customerName
			,	customerName: this.model.get('companyname') ? this.model.get('companyname') : this.model.get('firstname')
				//@property {Bool} showSalesRep
			,	showSalesRep: this.customerData.salesrep != null
				//@property {String} showSalesRep
			,	salesRepName: this.customerData.salesrep ? this.customerData.salesrep.name : ""
				//@property {String} showSalesRep
			,	salesRepPhone: this.customerData.salesrep ? this.customerData.salesrep.phone : ""
				//@property {String} showSalesRep
			,	salesRepEmail: this.customerData.salesrep ? this.customerData.salesrep.email : ""
			};

		}		
	});
});
/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// Overview.Home.View.js
// --------------------

define('Overview.Home.View.Extend'
,	[
		'Overview.Home.View'	
	,	'SC.Configuration'	
	,	'Backbone.CollectionView'
	,	'Overview.PurchaseTotals.View'
	,	'RecordViews.View'
	,	'Handlebars'

	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function(
		OverviewHomeView

	,	Configuration	
	,	BackboneCollectionView
	,	OverviewPurchaseTotalView
	,	RecordViewsView
	,	Handlebars	
	
	,	Backbone
	,	BackboneCompositeView
	,	jQuery
	,	_
	)
{
	'use strict';

	//@class OrderHistory.List.View.Extend view list of orders @extend Backbone.View
	return _.extend(OverviewHomeView.prototype ,{		

			//@property {Object} childViews
		childViews : _.extend(OverviewHomeView.prototype.childViews, {	
			'Overview.PurchaseTotal': function()
			{
				return !this.model.get("isperson") ? new OverviewPurchaseTotalView({ model: this.model }) : null;
			}		
		})

		//@method getContext @return Overview.Home.View.Context
	,	getContext: function ()
		{
			var isSCISIntegrationEnabled = Configuration.get('siteSettings.isSCISIntegrationEnabled', false);
			
			//@class Overview.Home.View.Context
			return {
				//@property {Boolean} collectionLengthGreaterThan0
				collectionLengthGreaterThan0: this.collection.length > 0
				//@property {Boolean} hasCustomerSupportURL
			,	hasCustomerSupportURL: !!this.customerSupportURL
				//@property {String} customerSupportURL
			,	customerSupportURL: this.customerSupportURL
				//@property {String} firstName
			,	firstName: this.model.get('firstname') ||  this.model.get('name') ||''
				//@property {Boolean} isSCISIntegrationEnabled
			,	isSCISIntegrationEnabled: this.isSCISIntegrationEnabled
				//@property {Boolean} showBackToAccount
			,	showBackToAccount: Configuration.get('siteSettings.sitetype') === 'STANDARD'
				// @property {Boolean} purchasesPermissions
			,	purchasesPermissions: isSCISIntegrationEnabled ? 'transactions.tranFind.1,transactions.tranPurchases.1' : 'transactions.tranFind.1,transactions.tranSalesOrd.1'
			};
		}
			
	});
});
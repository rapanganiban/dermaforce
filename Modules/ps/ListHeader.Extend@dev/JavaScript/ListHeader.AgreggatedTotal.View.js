define('ListHeader.AgreggatedTotal.View'
,	[
		'ListHeader.View'
	,	'GlobalViews.Pagination.View'
	,	'GlobalViews.ShowingCurrent.View'
	,	'AjaxRequestsKiller'

	,	'list_header_agreggatedtotal_view.tpl'

	,	'Backbone'
	,	'Backbone.CompositeView'
	,	'underscore'
	,	'jQuery'
	,	'Utils'
	,	'bootstrap-datepicker'
	,	'UrlHelper'
	]
,	function (
        ListHeaderView
	,	GlobalViewsPaginationView
	,	GlobalViewsShowingCurrentView
	,	AjaxRequestsKiller

	,	list_header_agreggatedtotal_view_tpl

	,	Backbone
	,	BackboneCompositeView
	,	_
	,	jQuery
	)
{
	'use strict';


	return ListHeaderView.extend({

		//@property {Function} template
		template: list_header_agreggatedtotal_view_tpl

	,   getContext: _.wrap(ListHeaderView.prototype.getContext, function getContext(fn) {
			var  context = fn.apply(this, _.toArray(arguments).slice(1));

            var range = null
            if (this.selectedRange)
            {
                //@class RangeFilter
                range = {
                    //@property {String} from
                    from: this.selectedRange.from || this.rangeFilterOptions.fromMin
                    //@property {String} to
                    ,	to: this.selectedRange.to || this.rangeFilterOptions.toMax
                };
            }

            //var currentDate = _.dateToString(new Date())
            //,	noMinDate = range.from === '1800-01-02' ? true : false
            //,	noMaxDate = range.to === currentDate ? true : false
            //,	noItemSelected = this.selectedFilter.value === '' ? true : false;

            var from = range.from ? new Date(range.from.replace(/-/g,'/')).getTime() : null;
           	var to = range.to ? new Date(range.to.replace(/-/g,'/')).getTime() : null;

            //if (noMinDate && noMaxDate && noItemSelected) {
            //	var csvUrl =  Backbone.history.location.origin +  _.getAbsoluteUrl('services/AgreggatedTotal.Download.Service.ss?&filter='+this.selectedFilter.value+'&sort='+this.selectedSort.value+'+&order='+this.order+'&download_type=csv');
            //} else {
            	var csvUrl =  Backbone.history.location.origin +  _.getAbsoluteUrl('services/AgreggatedTotal.Download.Service.ss?&filter='+this.selectedFilter.value+'&sort='+this.selectedSort.value+'+&order='+this.order+'&from='+from+'&to='+to+'&download_type=csv');
            //}
            	

			
           	
       
			return _.extend(context,{
                	csvUrl : csvUrl
			});
 
        })

	});

});
/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CreditCard
define('CreditCard.Edit.Form.View.Extend'
,	[	'CreditCard.Edit.Form.View'
	
	,	'SC.Configuration'	
	,	'Backbone'
	,	'underscore'
	]
,	function (
		CreditCardEditFormView

	,	Configuration	
	,	Backbone
	,	_
	)
{
	'use strict';

	// @class CreditCard.Edit.Form.View @extends Backbone.View
	return _.extend(CreditCardEditFormView.prototype , {

		
		getContext : _.wrap(CreditCardEditFormView.prototype.getContext, function(fn){
	       var currentContext = fn.apply(this, _.toArray(arguments).slice(1));

	       return _.extend(currentContext, {
	       		isNew: this.model.isNew()
	       });
	    }) 
	});
});

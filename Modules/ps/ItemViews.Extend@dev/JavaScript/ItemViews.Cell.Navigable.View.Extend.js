/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemViews
define('ItemViews.Cell.Navigable.View.Extend'
,	[	'ItemViews.Cell.Navigable.View'
	
	,	'Backbone'
	,	'Handlebars'
	,	'underscore'
	]
,	function (
		ItemViewsCellNavigableView

	,	Backbone
	,	Handlebars
	,	_
	)
{
	'use strict';

	//@class ItemViews.Navigable.View @extend Backbone.View
	return _.extend(ItemViewsCellNavigableView.prototype, {

		getContext : _.wrap(ItemViewsCellNavigableView.prototype.getContext, function(fn){
	       var currentContext = fn.apply(this, _.toArray(arguments).slice(1))
	       ,	link_attributes = {
	       			href: SC.ENVIRONMENT.siteSettings.wsdkcompleteloginurl+this.model.get('item').get('custitem_rsm_wpitemurl')
				};
			
	       return _.extend(currentContext, {
	       		itemURLAttributes: this.model.get('item').get('custitem_rsm_wpitemurl') != "" ?  new Handlebars.SafeString(_.objectToAtrributes(link_attributes)) : this.model.get('item').get('_linkAttributes')
	       });
	    })	
	
	});
});


/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemViews
define('ItemViews.Cell.Actionable.View.Extend'
,	[
		'ItemViews.Cell.Actionable.View'

	,	'Backbone'
	,	'Handlebars'
	,	'underscore'
	]
,	function (
		ItemViewsCellActionableView

	,	Backbone
	,	Handlebars
	,	_
	)
{
	'use strict';

	//@class ItemViews.Actionable.View @extend Backbone.View
	return _.extend(ItemViewsCellActionableView.prototype, {

		getContext : _.wrap(ItemViewsCellActionableView.prototype.getContext, function(fn){
	       var currentContext = fn.apply(this, _.toArray(arguments).slice(1))
		   ,	link_attributes = {
					href: SC.ENVIRONMENT.siteSettings.wsdkcompleteloginurl+currentContext.item.get('custitem_rsm_wpitemurl')
				};

	       return _.extend(currentContext, {
	       		linkAttributes: currentContext.item.get('custitem_rsm_wpitemurl') != "" ?  new Handlebars.SafeString(_.objectToAtrributes(link_attributes))	 : currentContext.item.get('_linkAttributes')
	       });
	    })

	});
});

/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderWizard.Module.PaymentMethod
define(
	'OrderWizard.Module.ShowPayments.Extend'
,	[	
		'OrderWizard.Module.ShowPayments'	
	,	'Backbone'
	,	'underscore'
	]
,	function (
		OrderWizardModuleShowPayments	
	,	Backbone	
	,	_
	)
{
	'use strict';
	//@class OrderWizard.Module.ShowPayments @extends Wizard.Module
	return _.extend(OrderWizardModuleShowPayments.prototype, {
	
		getContext : _.wrap(OrderWizardModuleShowPayments.prototype.getContext, function(fn){
	       var currentContext = fn.apply(this, _.toArray(arguments).slice(1));
	       return _.extend(currentContext, {
	       		//@property {LiveOrder.Model} model
	       		model: this.model
	       	,	showPONumber : 	this.model.get("options").custbody_sc_set_po_numer != ""
	       });
	    })  		
	});
});
// ReportingTotal.Router.js
// -----------------------
define(
	'ReportingTotal.Router'
,	[
		'ReportingTotal.View'
	,	'ReportingTotal.Collection'
	,	'ReportingTotal.Model'
	,	'ReportingTotal.FullList.Model'
	,	'Backbone'	
	]
,	function (
		View
	,	Collection
	,	Model
	,	ReportingTotalFullListModel
	,	Backbone
	)
{
		'use strict';

		return Backbone.Router.extend({

			routes: {
				'reportingtotalcompany': 'reportingtotal'
			}
			
		,	initialize: function (application)
			{
				this.application = application;
			}
			
			// list download History
		,	reportingtotal: function (options) 
			{
				options = (options) ? SC.Utils.parseUrlOptions(options) : {page: 1};
				
				options.page = options.page || 1;
				
				var model = new ReportingTotalFullListModel(),
				self = this;

				model.fetch().done(function(data){

					var filters = _.map(data.records, function(record){
					 return {
					 			value: record.sku
					 		,	name: record.sku+", "+record.name
					 		}
					});
					filters = _.uniq(filters, function(record){
					 return record.value
					});
					filters = _.union([{value: "",	name: "Filter by Item",	selected: true}], filters)
					var collection = new Collection()
					,	view = new View({
							application: self.application
						,	page: options.page
						,	collection: collection
						,	filters: filters
						});

					collection.on('reset', view.showContent, view);

					view.showContent();
				});					
			}	
		});
	}
);
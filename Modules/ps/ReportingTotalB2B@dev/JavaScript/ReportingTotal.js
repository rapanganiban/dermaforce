// ReportingTotal.js
// -----------------
// Defines the ReportingTotal  module (Model, Collection, View, Router)
define(
	'ReportingTotal'
,	[
		'ReportingTotal.View'
	,	'ReportingTotal.Router'	
	,	'underscore'
	, 	'Utils'
	]
,	function (View,  Router, Profile)
	{
		'use strict';

		
		return	{
			View: View
			
		,	Router: Router
		
		,	MenuItems: {
				parent: 'reporting'
			,	id: 'reportingtotalcompany'
			,	name: _('Sales by Item').translate()
			,	url: 'reportingtotalcompany'
			,	index: 1			
			} 
		
		,	mountToApp: function (application)
			{
				return new Router(application);
			}
		};
	}
);

// AgreggatedFullList.Model.js
// -----------------------
define('ReportingTotal.FullList.Model', ['Backbone', 'underscore', 'Utils'], function ( Backbone, _ )
{
    'use strict';

    return Backbone.Model.extend(
        {
            urlRoot: _.getAbsoluteUrl('services/ReportingTotal.FullList.Service.ss')
        });
});

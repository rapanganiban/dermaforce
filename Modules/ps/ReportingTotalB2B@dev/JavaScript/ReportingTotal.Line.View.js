/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ReportingTotal
define('ReportingTotal.Line.View'
,	[	'reporting_total_line.tpl'

	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	]
,	function (
		reporting_total_line_tpl

	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';

	//@class ReportingTotal.List.View view list of orders @extend Backbone.View
	return  Backbone.View.extend({
		//@property {Function} template
		template: reporting_total_line_tpl

		//@method getContext @return ReportingTotal.List.View.Context
	,	getContext: function ()
		{
			//@class ReportingTotal.List.View.Context

			return {
				//@property {String} name
				name: this.model.get('name')
				//@property {int} quantity
			,	quantity: this.model.get('quantity')
				// @property {int} total
			,	total: this.model.get('total')
				// @property {String} description
			,	description: this.model.get('description')	
				// @property {String} description
			,	sku: this.model.get('sku')	
				// @property {Date} description
			,	date: this.model.get('date')		
				// @property {String} description
			,	salesorder: this.model.get('salesorder')					
			};
		}
	});

});
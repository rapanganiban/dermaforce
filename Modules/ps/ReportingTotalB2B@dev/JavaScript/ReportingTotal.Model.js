// ReportingTotal.Model.js
// -----------------------
define('ReportingTotal.Model', ['Backbone', 'underscore', 'Utils'], function ( Backbone, _ )
{
	'use strict';
	
	return Backbone.Model.extend(
	{
		urlRoot: _.getAbsoluteUrl('services/ReportingTotal.Service.ss')
	});
});

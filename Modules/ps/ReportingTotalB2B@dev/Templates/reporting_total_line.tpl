{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<tr class="recordviews-row">
	<td class="recordviews-label">
		<span class="hidden-desktop">SKU: </span><span>{{sku}}</span>
	</td>
	<td class="recordviews-label">
		<span class="hidden-desktop">Name: </span><span>{{name}}</span>
	</td>
	<td class="recordviews-label">
		<span class="hidden-desktop">Transaction #: </span><span>{{salesorder}}</span>
	</td>
	<td class="recordviews-label">
		<span class="hidden-desktop">Date: </span><span>{{date}}</span>
	</td>	
	<td class="recordviews-label">
		<span class="hidden-desktop">Quantity: </span><span>{{quantity}}</span>
	</td>
	<td class="recordviews-label">
		<span class="hidden-desktop">Total: </span><span>${{total}}</span>
	</td>
</tr>
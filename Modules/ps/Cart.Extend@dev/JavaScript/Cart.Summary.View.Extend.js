/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Cart
define(
	'Cart.Summary.View.Extend'
,	[
		'Cart.Summary.View'	

	,	'underscore'
	,	'Backbone'
	,	'jQuery'
	]
,	function (

		CartSummaryView
	,	_
	,	Backbone
	,	jQuery

	)
{
	'use strict';

	// @class Cart.Summary.View @extends Backbone.View
	return _.extend(CartSummaryView.prototype, {

		/*events: {
			'click [data-action="proceed-to-checkout"]': 'validateQuantity'	
		}*/

		// @method initialize
		validateQuantity: function (e)
		{
			var itemsForms = jQuery(".cart-item-summary-item-list-actionable-qty-form")
			,	continue_enabled = true
				self = this;

			_.each(itemsForms, function($form){				
				var	internalid = jQuery($form).find('input[name*="internalid"]').val()
				,	line = self.model.get('lines').get(internalid)				
				,	item = line.get('item')
				,	quantityavailable = item.get("quantityavailable")
				,	new_quantity = jQuery($form).find('input[name*="quantity"]').val()

				if(quantityavailable < new_quantity){
					continue_enabled = false;
				}
			})

			if(!continue_enabled){				
				this.showWarningMessage(_("You are unable to continue because one or more items in your cart is not available in sufficient quantity. Please reduce your order.").translate())				
			}else{
				$("#btn-proceed-checkout").click()
			}
		}

	,	getContext : _.wrap(CartSummaryView.prototype.getContext, function(fn){
	       var currentContext = fn.apply(this, _.toArray(arguments).slice(1))
	       ,	lines = this.model.get("lines")
	       ,	continue_enabled = true
	       ,	items = [];


	       _.each(lines.models, function(line){
	       		var quantity = line.get("quantity")
	       		,	item = line.get("item")
	       		,	available = item.get("quantityavailable")

	       		if(quantity > available){
	       			continue_enabled = false;
	       			items.push(item.get("_sku"))
	       		}
	       });

	       return _.extend(currentContext, {	       	
	       		continue_disabled: !continue_enabled
	       	,	items_unablailables : 	JSON.stringify(items)
	       });
	    })  	

	});

});
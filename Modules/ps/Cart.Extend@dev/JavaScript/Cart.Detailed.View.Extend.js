/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Cart
define('Cart.Detailed.View.Extend'
,	[	'Cart.Detailed.View'

	,	'jQuery'
	,	'Backbone'
	,	'underscore'
	,	'Utils'

	]
,	function (
		CartDetailedView

	,	jQuery
	,	Backbone
	,	_
	)
{
	'use strict';


	// @class Cart.Detailed.View This is the Shopping Cart view @extends Backbone.View
	return _.extend(CartDetailedView.prototype, {

		getBreadcrumbPages: function ()
		{
			return "";
		}


	});
});

{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showBackToAccount}}
	<a href="/" class="profile-information-button-back">
		<i class="profile-information-button-back-icon"></i>
		{{translate 'Back to Account'}}
	</a>
{{/if}}

<div> 
	<div class="customer-invite-alert-placeholder" data-type="alert-placeholder"></div>
	<form class="contact_info">
		<div class="profile-information-row" data-input="firstname" data-validation="control-group" data-input="inviteemail">
			<label class="profile-information-label" for="firstname">{{translate 'Enroll Patient'}}</label>
			<p>Granting access to your patients to purchase skinbetter science&trade; products through our online store is easy.</p><br>
			<ul class="list-type">
				<li><p>Type in the patient's email address in the designated area below.</p></li>
				<li><p>Click on send invite.</p></li>
				<li><p>A link will then be emailed to the patient.</p></li>
				<li><p>Tell the patient to follow the instructions in the link provided.</p></li>
			</ul>
			<br>
			<p class="profile-email-bottom">It's as simple as that!</p>
				
			<div class="profile-information-group-form-controls" data-validation="control">
				<input type="email" id="inviteemail" name="inviteemail" placeholder="Enter a valid email address">
			</div>
		</div>
		<div class="profile-information-form-actions">
			<button type="submit" class="send-invite-button">Send Invite</button>
		</div>
	</form>
</div>

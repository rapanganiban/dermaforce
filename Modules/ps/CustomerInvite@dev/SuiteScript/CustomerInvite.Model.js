/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// CustomerInvite.Model.js
// --------------
// Defines the model used by the CustomerInvite.Service.ss service
define(
	'CustomerInvite.Model'
,	[
		'SC.Model'
	,	'Utils'
	,	'Application'
	
	]
,	function (

		SCModel
	,	Utils
	,	Application
	
	)
{
	'use strict';

	// @class CustomerInvite.Model Defines the model used by the CustomerInvite.Service.ss service
	// @extends SCModel
	return SCModel.extend({

		//@property {String} name
		name: 'CustomerInvite'

		//@method get
		//@param {String} email
		//@return {boolean}
	,	sendInvite: function (email)
		{	
			var exits = false,
			customerid = nlapiGetUser(),
			customerEmail = customer.getFieldValues(['email', 'internalid']),
			status = {success:true, exits : false, same : false},
			filter = [
				new nlobjSearchFilter("custrecord_sc_sco_invited_email", null,"is",email),				
			],
			columns = [
				new nlobjSearchColumn("custrecord_sc_sco_accepted_invitation"),
				new nlobjSearchColumn("custrecord_sc_sco_customer")
			],
			search = nlapiSearchRecord('customrecord_sc_sco_customer_invite',null,filter,columns),
			filterB2B = [
                new nlobjSearchFilter("email",null,'is',email),
                new nlobjSearchFilter("isperson",null,'is',"F")
			],
            searchB2B = nlapiSearchRecord("customer", null, filterB2B);

			if(customerEmail.email == email || searchB2B){
                status.success = false;
                status.exits = true;
                status.same = true;
                exits = true;
			}else if(search){
				if(search.length > 0){
					if(search[0].getValue("custrecord_sc_sco_accepted_invitation") == "F"){
						exits = true;						
						var record = nlapiLoadRecord('customrecord_sc_sco_customer_invite',search[0].getId(),{recordmode: 'dynamic'});
						record.setFieldValue('custrecord_sc_sco_resend_inivation','T');
						search[0].getValue("custrecord_sc_sco_customer") == customerid ? status.resend = true : record.setFieldValue('custrecord_sc_sco_customer',customerid);
						nlapiSubmitRecord(record);						
					}else{
						exits = true;
						status.exits = true
					}	
				}
			}
			
			if(!exits){
				var record = nlapiCreateRecord('customrecord_sc_sco_customer_invite')
				try{
					record.setFieldValue("custrecord_sc_sco_customer", customerid);
					record.setFieldValue("custrecord_sc_sco_invited_email",email);
					nlapiSubmitRecord(record);
				}catch(e){
					nlapiLogExecution('ERROR', 'error', e);
					status.success = false
				}				
			}
			return status;
		}		
	});
});
/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CustomerInvite
define(
		'CustomerInvite.Model'
,	[		
		'Backbone'
	]
,	function (

		Backbone
	)
{
	'use strict';

	//@class Quote.Model 
	//@extends Backbone.Model
	return Backbone.Model.extend({

		//@property {String} urlRoot
		urlRoot: 'services/CustomerInvite.Service.ss'
		
		 // validation: properties
        // https://github.com/thedersen/backbone.validation
        ,	validation: {
            inviteemail: {
                required: true
            }
        }



	});
});
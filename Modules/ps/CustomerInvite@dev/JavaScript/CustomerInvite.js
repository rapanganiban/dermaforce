/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// CustomerInvite.js
// -----------------
// Defines the CustomerInvite module (Model, Views, Router)
// @module CustomerInvite
define(
		'CustomerInvite'
,	[	
		'CustomerInvite.Router'	
	,	'Profile.Model'	
	,	'underscore'
	,	'Utils'
	]
,	function (
		Router
	,	Profile	
	,	_
	)
{
	'use strict';	

	var profile = Profile.getInstance();

	return	{

		// @property {MenuItem} MenuItems
		MenuItems: function () 
		{	
			
			return 	!profile.get("isperson") ? {	
				id: 'customerinvite'
			,	name: _('Enroll Patient').translate()
			,	url: 'customerinvite'
			,	index: 5
			} : null;
		}

		// @method mountToApp
	,	mountToApp: function (application)
		{
			return !profile.get("isperson") ? new Router(application) : null;
		}
	};
});

/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// Receipt.Router.js
// -----------------------
// Router for handling receipts
define('CustomerInvite.Router'
,	[	'CustomerInvite.View'
	,	'AjaxRequestsKiller'
	,	'CustomerInvite.Model'

	,	'Backbone'
	]
,	function (
		CustomerInviteView
	,	AjaxRequestsKiller
	,	Model

	,	Backbone
	)
{
	'use strict';

	return Backbone.Router.extend({

		routes: {
			'customerinvite': 'inviteView'
		}

	,	initialize: function (application)
		{
			this.application = application;
		}

		// view receipt's detail
	,	inviteView: function (id)
		{
			var model = new Model()
			,	view = new CustomerInviteView({
					application: this.application				
				,	model: model
				});

			view.showContent();			
		}
	});
});
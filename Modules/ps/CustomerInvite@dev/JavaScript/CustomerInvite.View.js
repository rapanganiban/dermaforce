/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CustomerInvite
define('CustomerInvite.View'
,	[
		'SC.Configuration'
	,	'customer_invite.tpl'
	,	'GlobalViews.Message.View'
		
	,	'Backbone'
	,	'jQuery'
	,	'underscore'	

	]
,	function(
		Configuration
	,	customerinvite_tpl
	,	GlobalViewsMessageView	
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';
	return Backbone.View.extend({

		// @property {Function} template
		template: customerinvite_tpl

	,	title: 'Enroll Patient'

	,	events:{
			'submit form': 'saveForm'
		}

	,	 bindings: {
            '[name="inviteemail"]': 'inviteemail'
        }
        	
		// @method initialize
	,	initialize: function (options)
		{			
			this.application = options.application;
			this.model.on('save', this.showSuccess);
		}

	,	showSuccess: function (response)
		{
			var global_view_message;

			if(response.get("exits") && !response.get("same")){
				global_view_message  = new GlobalViewsMessageView({
											message: _('This patient already has an account on file. Please contact your Sales Representative or the Concierge Service for assistance.').translate()
										,	type: 'error'
										,	closable: true
									});	
			} else if(response.get("exits") && response.get("same")){
                global_view_message  = new GlobalViewsMessageView({
                    message: _('This email address is already is use.').translate()
                    ,	type: 'error'
                    ,	closable: true
                });
            }else{
				global_view_message  = new GlobalViewsMessageView({
											message: response.get("resend") ? _('The Email invitation has been resent!').translate() :_('The Email invitation has been sent!').translate()
										,	type: 'success'
										,	closable: true
									});	
			}
					
			jQuery('[data-type="alert-placeholder"]').append(global_view_message.render().$el.html());
			jQuery('#inviteemail').val("")
		}
		//@method getSelectedMenu
	,	getSelectedMenu: function ()
		{
			return 'customerinvite';
		}
	,	getBreadcrumbPages: function ()
		{
			return {
				text: this.title
			,	href: '/customerinvite'
			};
		}
	,	getContext: function()
		{
			// @class Profile.Information.View.Context
			return {
				showBackToAccount: Configuration.get('siteSettings.sitetype') === 'STANDARD'
			};
		}
	});
});
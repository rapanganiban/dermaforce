/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// Quote.js
// -----------------
// Defines the Return Authorization module (Model, Collection, Views, Router)
// @module Quote
define(
		'Quote.Extend'
,	[	
		'Quote'

	,	'underscore'
	,	'Utils'
	]
,	function (
		Quote

	,	_
	)
{
	'use strict';

	_.extend(Quote,{
		MenuItems : null
		, mountToApp : null
	});	
});

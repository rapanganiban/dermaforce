/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module SiteSettings
// Pre-processes the SiteSettings to be used on the site
define(
	'SiteSettings.Model.Extend'
,	[	'SiteSettings.Model'
	,	'SC.Model'
	,	'Application'
	,	'underscore'
	,	'Utils'
	]
,	function (
		SiteSettings
	,	SCModel
	,	Application
	,	_
	,	Utils
	)
{
	'use strict';

	// @class Account.Model
	Application.on('after:SiteSettings.get', function (Model, result) {
		var ck = "";
		if(result.touchpoints.serversync.indexOf("ck") > -1){
			ck = "?ck="+result.touchpoints.serversync.split("ck=")[1]
		}	
		var checkoutURL =result.touchpoints.customercenter.split(".com")[0]
		
		return _.extend(result.touchpoints, {
			home : result.wsdkcancelcarturl ? result.wsdkcancelcarturl+ck : result.touchpoints.home,
			logout : result.wsdkcancelcarturl ? result.wsdkcancelcarturl : result.touchpoints.logout,
			welcome : result.wsdkcancelcarturl ? result.wsdkcancelcarturl+ck : result.touchpoints.welcome,
			continueshopping : result.wsdkcancelcarturl ? result.wsdkcancelcarturl+ck : result.touchpoints.continueshopping,
			viewcart : checkoutURL+".com/checkout/cart.ssp"
		});		
	});
});
/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Footer
define(
	'Footer.Simplified.View'
,	[
		'footer_simplified.tpl'
	,	'Backbone'
	]
,	function(
		footer_simplified_tpl
	,	Backbone
	)
{
	'use strict';

	// @class Footer.Simplified.View @extends Backbone.View
	return Backbone.View.extend({
		//@property {Function} template
		template: footer_simplified_tpl
	,	getContext: function()
		{
			// @class Header.Logo.View.Context
			return {
				socialFacebook: _.getAbsoluteUrl('img/facebook.png')
			,	socialInstagram: _.getAbsoluteUrl('img/instagram.png')
			,	socialPinterest: _.getAbsoluteUrl('img/pinterest1.png')
			,	socialTwitter: _.getAbsoluteUrl('img/twitter1.png')
			,	socialYoutube: _.getAbsoluteUrl('img/youtube1.png')
			,	valueSession: jQuery('.menu-item-512 a').first().attr("href").substr(jQuery('.menu-item-512 a').first().attr("href").indexOf('?')+1)
			,	wsdklink: SC.ENVIRONMENT.siteSettings.wsdkcompleteloginurl
			};
		}
	});
});

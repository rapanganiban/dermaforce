{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="footer-content">
	<div class="footer-container row collapse">
      <div class="small-12 columns">
        <div class="row">
          <div class="footer-menu small-12 medium-3 columns">
            <div class="menu-footer-menu-container">
                <ul id="menu-footer-menu" class="menu">
                    <li id="menu-item-693" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-693"><a href="{{wsdklink}}about/">About</a></li>
                    <li id="menu-item-214" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-214"><a href="{{wsdklink}}products/">Our Products</a></li>
                    <li id="menu-item-696" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-696"><a href="{{wsdklink}}frequently-asked-questions/">FAQs</a></li>
                    <li id="menu-item-695" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-695"><a href="{{wsdklink}}how-to-buy/">How To Buy</a></li>
                    <li id="menu-item-694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-694"><a href="{{wsdklink}}find-a-professional/">Find a Professional</a></li>
                    <li id="menu-item-698" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-470 current_page_item menu-item-698"><a href="{{wsdklink}}find-a-sales-rep/">Find a Sales Representative</a></li>
                    <li id="menu-item-697" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-697"><a href="{{wsdklink}}career-opportunities/">Career Opportunities</a></li>
                    <li id="menu-item-697" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-697"><a href="{{wsdklink}}contact/">Contact</a></li>
                </ul>
            </div>
          </div>
          <div id="social-container" class="small-12 medium-5 columns">
            Connect With Us
            <ul class="social-icons">
                <li>
                    <a target="_blank" href="https://www.facebook.com/MySkinBetter"><img src="{{socialFacebook}}" /></a>
                </li>
                <li>
                    <a target="_blank" href="https://www.pinterest.com/skinbetter/"><img src="{{socialTwitter}}" /></a>
                </li>
                <li>
                    <a target="_blank" href="https://twitter.com/skinbetter"><img src="{{socialInstagram}}" /></a>
                </li>
                <li>
                    <a target="_blank" href="https://www.instagram.com/skinbetter/"><img src="{{socialPinterest}}" /></a>
                </li>
                <li>
                    <a target="_blank" href="https://www.youtube.com/channel/UCRv11jKSFKLeomXiSgSRlVA"><img src="{{socialYoutube}}" /></a>
                </li>
            </ul>
          </div>
          <div class="small-12 medium-4 columns">
            <!--div style="margin-bottom: 10px;">Already have an account?</div-->

            <a class="button button--full-width" href="https://store.skinbetter.com/checkout/checkout.ssp?is=login&sc=1&login=T&reset=T#login-register">Professional Login</a>
            <a class="button button--full-width" href="https://store.skinbetter.com/checkout/checkout.ssp?is=login&sc=1&login=T&reset=T&origin={{wsdklink}}products#login-register">Consumer Login</a>

          </div>
        </div>
        <div class="row">
          <div class="copyright small-12 columns">
            Copyright &copy; 2016 |
            <a href="{{wsdklink}}privacy-policy/">Privacy Policy</a> |
            <a href="{{wsdklink}}terms-of-service/">Terms &amp; Conditions</a> |
            <a href="{{wsdklink}}return-policy/">Return Policy</a> |
            <a href="{{wsdklink}}shipping-policy/">Shipping Policy</a>
          </div>
        </div>
      </div>
    </div>
	<!--Start Modal-->
	<div class="modal fade" id="modalTerms" role="dialog">
    	<div class="modal-dialog">

	<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Welcome to the skin<b>better</b> science&trade; Personal Portal.</h4>
				</div>
				<br class="modal-body">
                    <p>You are able to enroll in this secure portal because your physician, healthcare professional or an authorized provider has granted you exclusive access to our online ordering system. All products acquired through this secure portal are for your personal use only and not for resale.</p>
                    </br>
					<p>Enrolling through this secure portal enables you to: (i) purchase skin<b>better</b> science products online; and (ii) track orders to monitor shipping and delivery.</p>
                    </br>
                    <p>We follow industry best practices to maintain the safety of your account information to ensure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed. Your account information is contained behind secured networks and all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. For more insight, you may also want to read our <a href="{{wsdklink}}privacy-policy">Privacy Policy</a>. </p>
                    </br>
                    <p>By enrolling your account through this Portal, you acknowledge and consent that we use the information that you provide us to supply you with educational, promotional, and other informational materials about our products, programs, and services. Subsequent to this enrollment and opt-in, you may withdraw your consent at anytime by e-mailing us at <a href="mailto:support@skinbetter.com">support@skinbetter.com</a>.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
      
		</div>
	</div>
</div>
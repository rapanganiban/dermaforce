/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Wizard
define('Wizard.Step.Extend'
,	[	'Backbone'
	,	'underscore'
	,	'jQuery'

	,	'Wizard.Step'
	,	'Utils'
	]
,	function (
		Backbone
	,	_
	,	jQuery

	,	WizardStep
	)
{
	'use strict';

	// @class Wizard.Step Step View, Renders all the components of the Step @extends {Backbone.View}
	return _.extend(WizardStep.prototype, {
		

		// @method submit Calls the submit method of each module. Calls our save function
		// and asks the wizard to go to the next step
		submit: function (e)
		{
			// Disables the navigation Buttons
			e && this.disableNavButtons();

			if( this.options.wizard.currentStep == "shipping/method" && jQuery("#delivery-instructions")){				
				_.extend(this.wizard.model.get("options") , { custbody_sc_sco_scpecial_instructions:jQuery("#delivery-instructions").val()})
				//this.wizard.model.save()
			}else if(this.options.wizard.currentStep == "billing" && jQuery("#purchase-order-number")){
				_.extend(this.wizard.model.get("options")  , { custbody_sc_set_po_numer:jQuery("#purchase-order-number").val()})				
			}
			// Calls the submit method of the modules and collects errors they may have
			var promises = [];

			_.each(this.moduleInstances, function (module_instance)
			{
				if (module_instance.isActive())
				{
					promises.push(
						module_instance.submit(e)
					);
					module_instance.disableInterface();
				}
			});

			var self = this;
			jQuery.when.apply(jQuery, promises).then(
				// Success Callback
				function ()
				{
					//NOTE: Here the order between then and always have changed, because when certain module wanted to disable nav buttons this always re-enable them!
					//Validate this change!
					self.save().always(function ()
					{
						self.enableNavButtons();
					}).done(function ()
					{
						self.wizard.goToNextStep();
					}).fail(function (error)
					{
						self.submitErrorHandler(error);
					});
				}
				// Error Callback
			,	function (error)
				{
					self.submitErrorHandler(error);
				}
			);
		}	
	});
});

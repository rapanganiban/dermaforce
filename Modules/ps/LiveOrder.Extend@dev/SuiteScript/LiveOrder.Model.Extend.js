/*
    © 2016 NetSuite Inc.
    User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
    provided, however, if you are an authorized user with a NetSuite account or log-in, you
    may use this code subject to the terms that govern your access and use.
*/

/* jshint -W053 */
// We HAVE to use "new String"
// So we (disable the warning)[https://groups.google.com/forum/#!msg/jshint/O-vDyhVJgq4/hgttl3ozZscJ]
/* global order,customer */
// @module LiveOrder
define(
    'LiveOrder.Model.Extend'
,   [   'LiveOrder.Model'
    ,   'SC.Model'
    ,   'Application'
    ,   'Profile.Model'
    ,   'StoreItem.Model'
    ,   'Utils'
    ,   'underscore'
    ]
,   function (
        LiveOrder
    ,   SCModel
    ,   Application
    ,   Profile
    ,   StoreItem
    ,   Utils
    ,   _
    )
{
    'use strict';

    // @class Account.Model
    Application.on('after:LiveOrder.get', function (Model, result) {
        sitesettings = session.getSiteSettings();
        var field = customer.getFieldValues(['isperson'])
        ,   defaultShipmethod = null
        if(field.isperson){
            defaultShipmethods = nlapiLookupField("customrecord_sc_shipping_items_rules",2,"custrecord_sc_shipping_items")
        }else{
            defaultShipmethods = nlapiLookupField("customrecord_sc_shipping_items_rules",1,"custrecord_sc_shipping_items")
        }  
        var checkoutURL = sitesettings.touchpoints.customercenter.split(".com")[0]
        return _.extend(result, {
            touchpoints : _.extend(result.touchpoints, {
                            home : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl : result.touchpoints.home,
                            logout : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl : result.touchpoints.logout,
                            welcome : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl : result.touchpoints.welcome,
                            continueshopping : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl : result.touchpoints.continueshopping,
                            viewcart : checkoutURL+".com/checkout/cart.ssp"
                         }),
            shipmethod : defaultShipmethod != null && defaultShipmethod != "" && result.shipmethod != null ? defaultShipmethod : result.shipmethod
        });
    });

    _.extend( require('LiveOrder.Model') , {

        getShipMethods: function (order_fields)
        {
            var shipmethods = _.map(order_fields.shipmethods, function (shipmethod)
            {
                var rate = Utils.toCurrency(shipmethod.rate.replace( /^\D+/g, '')) || 0;

                return {
                    internalid: shipmethod.shipmethod
                ,   name: shipmethod.name
                ,   shipcarrier: shipmethod.shipcarrier
                ,   rate: rate
                ,   rate_formatted: shipmethod.rate
                };
            });

            var shipmethodsByItems = []
            ,   defaultShipmethods = []
            ,   field = customer.getFieldValues(['isperson'])
            
            defaultShipmethods = nlapiLookupField("customrecord_sc_shipping_items_rules",3,"custrecord_sc_shipping_items").split(",")
            

            if(field.isperson){
                defaultShipmethods.push(nlapiLookupField("customrecord_sc_shipping_items_rules",2,"custrecord_sc_shipping_items"))
            }else{
                defaultShipmethods.push(nlapiLookupField("customrecord_sc_shipping_items_rules",1,"custrecord_sc_shipping_items"))
            }        
                
            _.each(defaultShipmethods, function (default_ship)
            {
                    var method = _.find(shipmethods, function(shipmethods){
                        return shipmethods.internalid == default_ship
                    });
                    if(method)
                        shipmethodsByItems.push(method);
                 
            });

            if(shipmethodsByItems.length > 0 )
                shipmethodsByItems = _.sortBy(shipmethodsByItems, 'rate');
           
           return shipmethodsByItems.length > 0 ? shipmethodsByItems : shipmethods;
        }

        ,   submit: function ()
        {
            var payment_method = order.getPayment(['thankyouurl'])
            ,   paypal_address = _.find(customer.getAddressBook(), function (address){ return !address.phone && address.isvalid === 'T'; })
            ,   confirmation = order.submit();
            // We need remove the paypal's address because after order submit the address is invalid for the next time.
            //this.removePaypalAddress(paypal_address);

            context.setSessionObject('paypal_complete', 'F');

            if (this.isMultiShippingEnabled)
            {
                order.setEnableItemLineShipping(false); // By default non order should be MST
            }

            if (confirmation.statuscode !== 'redirect')
            {
                confirmation = _.extend(this.getConfirmation(confirmation.internalid), confirmation);
            }

            return confirmation;
        }
    });    
});

/**
 * Created by ptrecca on 3/28/17.
 */

function createInviteRecord(request, response){

    var email = request.getParameter('email');
    var b2bId = request.getParameter('b2bId');
    var customer = request.getParameter('customer');
    var recordid;
    try{
        var record = nlapiCreateRecord('customrecord_sc_sco_customer_invite')
        record.setFieldValue("custrecord_sc_sco_customer",  b2bId);
        record.setFieldValue("custrecord_sc_new_customer",customer);
        record.setFieldValue("custrecord_sc_sco_accepted_invitation","T");
        recordid = nlapiSubmitRecord(record);
    }catch(e){
        nlapiLogExecution('ERROR', 'error', e);
    }

    response.setContentType('json');
    response.write(recordid);

}

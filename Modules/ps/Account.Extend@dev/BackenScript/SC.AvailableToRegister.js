/**
 * Created by ptrecca on 3/28/17.
 */

function isAvailableToRegister(request, response){

    var email = request.getParameter('email');
    var b2bId = request.getParameter('b2bId');
    var enable_register = false;
    try {
        var filter = [
                new nlobjSearchFilter("internalid", null, "is", b2bId),
                new nlobjSearchFilter("isperson", null, "is", "F")
            ],
            column = [new nlobjSearchColumn("internalid")],
            search = nlapiSearchRecord('customer', null, filter, column);

        if (search) {
            if (search.length > 0) {
                filter = [
                    new nlobjSearchFilter("email", null, "is", email),
                    new nlobjSearchFilter("isperson", null, "is", "T")
                ];
                search = nlapiSearchRecord('customer', null, filter, null);
                enable_register = true;
                if (search) {
                    if (search.length > 0) {
                        enable_register = false;
                    }
                }
            }
        }
    } catch(e){
        nlapiLogExecution("ERROR","error",e);
    }

    response.setContentType('json');
    response.write(enable_register);
}

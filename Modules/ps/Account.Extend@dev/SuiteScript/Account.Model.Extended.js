/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module Account
// ----------
// Handles account creation, login, logout and password reset
// module Account
define(
	'Account.Model.Extended'
,	[
		'Account.Model' 
	,	'SC.Model'	
	,	'Application'	
	,	'Utils'
	,	'underscore'
	]
,	function (
		Account	
	,	SCModel
	,	Application
	,	Utils
	,	_		
	)
{

	// @class Account.Model
	Application.on('before:Account.register', function (Model, user_data) {	
		
		var enable_register = false;

		if(user_data.termsandconditions != 'on'){
			var message = "You must agree to terms and conditions.";
			throw message;	
		}

		try{
			if(user_data.b2bId){
                var url = nlapiResolveURL('SUITELET', 'customscript_sc_is_regiter_available', 'customdeploy_sc_is_regiter_available', true)
                    ,	response = nlapiRequestURL(url, user_data)
                    ,	result   = {};
                if(response.getBody())
                {
                    enable_register = response.getBody();
                }
			} else {
                var filter = [
                        new nlobjSearchFilter("custrecord_sc_sco_invited_email", null, "is", user_data.email),
                        new nlobjSearchFilter("custrecord_sc_sco_accepted_invitation", null, "is", "F")
                    ],
                    search = nlapiSearchRecord('customrecord_sc_sco_customer_invite', null, filter, null);

                if (search) {
                    if (search.length > 0) {
                        nlapiSubmitField('customrecord_sc_sco_customer_invite', search[0].getId(), 'custrecord_sc_sco_accepted_invitation', 'T');
                        enable_register = true
                    }
                }
            }
			
		}catch(e){
			nlapiLogExecution('ERROR', 'error', e);
		}

		if(!enable_register || enable_register == "false"){
			var message = "The email address was not enabled to create an account." 
			throw message;
		}
	});

	// @class Account.Model
	Application.on('after:Account.login', function (Model, result) {	
		sitesettings = session.getSiteSettings();        
        var ck = "";
        if(result.touchpoints.serversync.indexOf("ck") > -1){
            ck = "?ck="+result.touchpoints.serversync.split("ck=")[1]
        }   
        var checkoutURL = sitesettings.touchpoints.customercenter.split(".com")[0]
        return _.extend(result.touchpoints, {
            home : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl+ck : result.touchpoints.home,
            logout : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl : result.touchpoints.logout,
            welcome : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl+ck : result.touchpoints.welcome,
            continueshopping : sitesettings.wsdkcancelcarturl ? sitesettings.wsdkcancelcarturl+ck : result.touchpoints.continueshopping,
            viewcart : checkoutURL+".com/checkout/cart.ssp"
        });	

	});	

	// @class Account.Model
	Application.on('after:Account.register', function (Model, result, user_data) {
        var inviteId;
        nlapiLogExecution("ERROR","bub",JSON.stringify(user_data));
		if(user_data.b2bId) {
            inviteId = user_data.b2bId;
            try{
                var url = nlapiResolveURL('SUITELET', 'customscript_sc_create_invite_record', 'customdeploy_sc_create_invite_record', true) + "&b2bId=" + user_data.b2bId + "&email="+ user_data.email+ "&customer=" + result.user.internalid
                ,	response = nlapiRequestURL(url);
                if(response.getBody() && parseInt(response.getBody()) != NaN){
                    nlapiSubmitField('customrecord_sc_sco_customer_invite',response.getBody(), 'custrecord_sc_sco_invited_email', user_data.email);
				}
            }catch(e){
                nlapiLogExecution('ERROR', 'error', e);
            }
        } else {
            var filter = [
                    new nlobjSearchFilter("custrecord_sc_sco_invited_email", null, "is", user_data.email),
                    new nlobjSearchFilter("custrecord_sc_sco_accepted_invitation", null, "is", "T"),
                    new nlobjSearchFilter("custrecord_sc_new_customer", null, "isempty", "T")
            ],
            columns = [new nlobjSearchColumn("custrecord_sc_sco_customer")]
            search = nlapiSearchRecord('customrecord_sc_sco_customer_invite', null, filter, columns),
            customer = session.getCustomer();

            nlapiSubmitField('customrecord_sc_sco_customer_invite', search[0].getId(), 'custrecord_sc_new_customer', result.user.internalid);
            inviteId = search[0].getValue("custrecord_sc_sco_customer");
		}

		customer.updateProfile({"customfields" : {"custentity_sc_terms_and_conditions" : "T" ,"custentity_sc_invite_sent":inviteId}});

	});

	_.extend( require('Account.Model') , {

			get: function ()
		{							
			var	field = customer.getFieldValues(['isperson'])
			,	custom_field = customer.getCustomFieldValues()
			,	terms_not_accepted = _.find(custom_field, function(field){
				if(field.name == "custentity_sc_terms_and_conditions" && field.value == "F")
						return field
			});		

			return {terms : terms_not_accepted ? "F" : "T", isperson : field.isperson};
		}

		,	updateCustomer: function(terms, newpassword,oldpassword){
			
			try{
				var login = nlapiGetLogin()				
				,	result =login.changePassword(oldpassword, newpassword);			
				
				customer.updateProfile({"customfields" : {"custentity_sc_terms_and_conditions" : "T", "custentity_sbs_enrollment_status" :"2"}})
				
			}catch(e){
				throw new Error('The current password you supplied is incorrect.');
			}			
			return {success : true}
		}
		
	});

});
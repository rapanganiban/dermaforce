/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ReorderItems
define(
	'ReorderItems.List.View.Extend'
,	[	
		'ReorderItems.List.View'
	,	'SC.Configuration'
	,	'GlobalViews.Pagination.View'
	,	'GlobalViews.ShowingCurrent.View'
	,	'Tracker'
	,	'LiveOrder.Model'
	,	'Backbone.CompositeView'
	,	'Backbone.CollectionView'
	,	'ItemViews.Cell.Actionable.View'
	,	'ReorderItems.Actions.Quantity.View'
	,	'ReorderItems.Actions.AddToCart.View'
	,	'GlobalViews.Message.View'
	,	'ErrorManagement'
	,	'Handlebars'

	,	'reorder_items_list.tpl'

	,	'Backbone'

	,	'underscore'
	,	'jQuery'
	]
,	function (

		ReorderItemsListView
	,	Configuration
	,	GlobalViewsPaginationView
	,	GlobalViewsShowingCurrentView
	,	Tracker
	,	LiveOrderModel
	,	BackboneCompositeView
	,	BackboneCollectionView
	,	ItemViewsActionableView
	,	ReorderItemsActionsQuantityView
	,	ReorderItemsActionsAddToCartView
	,	GlobalViewsMessageView
	,	ErrorManagement
	,	Handlebars

	,	reorder_items_list_tpl

	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';

	//@class ReorderItems.List.View @extends Backbone.View
	return _.extend(ReorderItemsListView.prototype, {
	
		//@method addToCart add to cart an item, the quantity is written by the user on the input and the options are the same that the ordered item in the previous order
		addToCart: function (e)
		{
			e.preventDefault();

			var	self = this
			,	line_id = this.$(e.target).data('line-id')
			,	$form = this.$(e.target).closest('[data-type="order-item"]')
			,	$alert_placeholder = $form.find('[data-type=alert-placeholder]')
			,	$quantity = $form.find('input[name=item_quantity]')
			,	selected_line = this.collection.get(line_id)
			,	item_to_cart = selected_line.get('item')
			,	quantity = isNaN(parseInt($quantity.val(), 10)) ? 0 : parseInt($quantity.val(), 10);

			if (quantity)
			{
				item_to_cart.set('quantity', quantity);

				LiveOrderModel.getInstance().addItem(item_to_cart).done(function ()
				{
					self.trackEventReorder(item_to_cart);

					$alert_placeholder.show().empty();

					var message;

					if (quantity > 1)
					{
						message = _('$(0) Items successfully added to <a href="#" data-touchpoint="viewcart">your cart</a><br/>').translate(quantity);
					}
					else
					{
						message = _('Item successfully added to <a href="#" data-touchpoint="viewcart">your cart</a><br/>').translate();
					}

					message = new GlobalViewsMessageView({
							message: message
						,	type: 'success'
						,	closable: true
					}).render().$el;

					$alert_placeholder.html(message);

					

				}).fail(function (jqXhr)
				{
					jqXhr.preventDefault = true;

					var message = new GlobalViewsMessageView({
						message: ErrorManagement.parseErrorMessage(jqXhr, self.application.getLayout().errorMessageKeys)
					,	type: 'error'
					,	closable: true
					}).render().$el;

					$alert_placeholder.show().empty().html(message);
				});
			}
			else
			{
				var message = new GlobalViewsMessageView({
						message: _('The number of items must be positive.').translate()
					,	type: 'error'
					,	closable: true
				}).render().$el;

				$alert_placeholder.show().empty().html(message);
			}
		}
	

	});

});

/*
* Get custom field for the customer
* 
*/
function GetCustomerSalesOrderTotals(params)
{

	var customerid = params.getParameter("customerid")	
	,	ordersByDate = [
		{month :"Jan", value: 0},
		{month :"Feb", value: 0},
		{month :"Mar", value: 0},
		{month :"Apr", value: 0},
		{month :"May", value: 0},
		{month :"Jun", value: 0},
		{month :"Jul", value: 0},
		{month :"Aug", value: 0},
		{month :"Sep", value: 0},
		{month :"Oct", value: 0},
		{month :"Nov", value: 0},
		{month :"Dec", value: 0},
	]
	,	consumersordersByDate = [
		{month :"Jan", value: 0},
		{month :"Feb", value: 0},
		{month :"Mar", value: 0},
		{month :"Apr", value: 0},
		{month :"May", value: 0},
		{month :"Jun", value: 0},
		{month :"Jul", value: 0},
		{month :"Aug", value: 0},
		{month :"Sep", value: 0},
		{month :"Oct", value: 0},
		{month :"Nov", value: 0},
		{month :"Dec", value: 0},
	]
	,	result = {
		orders : 0,
		total : 0,
		consumersOrders : 0,
		consumersTotals : 0,
		consumersOrdersByDate : consumersordersByDate,
		ordersByDate : ordersByDate,
		salesrep : null,
		success : true
	}
	,	customerFilter = [ new nlobjSearchFilter("entity",null,"is",customerid)]
	,	consumersFilter = [ new nlobjSearchFilter("custentity_sc_invite_sent","customer","is",customerid)]
	,	customerSO = nlapiSearchRecord('transaction',"customsearch_sc_search_customer_totals",customerFilter)
	,	consumersSO = nlapiSearchRecord('transaction',"customsearch_sc_search_customer_totals",consumersFilter)
	,	customerSOByDate = nlapiSearchRecord('transaction',"customsearch_sc_sales_by_date",customerFilter)
	,	consumersSOByDate = nlapiSearchRecord('transaction',"customsearch_sc_sales_by_date",consumersFilter);

	if(customerSO){
		if(customerSO.length > 0){
			result.total = customerSO[0].getValue("internalid",null,"COUNT")
			result.orders = customerSO[0].getValue("amount",null,"SUM")
		}
	}

	if(consumersSO){
		if(consumersSO.length > 0){
			result.consumersTotals = consumersSO[0].getValue("internalid",null,"COUNT")
			result.consumersOrders = consumersSO[0].getValue("amount",null,"SUM")
		}
	}

	if(customerSOByDate){
		if(customerSOByDate.length > 0){
			for (var i = 0; i < customerSOByDate.length; i++) {				
				var monthNumber =  getMonthNumber(customerSOByDate[i].getValue("trandate",null,"group").split("-")[1]);							
				result.ordersByDate[monthNumber].value= customerSOByDate[i].getValue("amount",null,"sum")
			};
			
		}
	}

	if(consumersSOByDate){
		if(consumersSOByDate.length > 0){
			for (var i = 0; i < consumersSOByDate.length; i++) {
				var monthNumber =  getMonthNumber(consumersSOByDate[i].getValue("trandate",null,"group").split("-")[1])				
				result.consumersOrdersByDate[monthNumber].value = consumersSOByDate[i].getValue("amount",null,"sum")
			};
		}
	}

	var	salesrepId = nlapiLookupField("customer",customerid,"salesrep")
    ,	salesrepName = nlapiLookupField("customer",customerid,"salesrep",true);
    
	
    if(salesrepId && salesrepId != "" && salesrepId != null){
    	result.salesrep = {
    		email : nlapiLookupField("employee",salesrepId,"email"),
    		name : salesrepName,
    		phone : nlapiLookupField("employee",salesrepId,"mobilephone")
    	}
    } 
            

	response.setContentType("JSON");
 	response.write(JSON.stringify(result));
}

//given a date it returns the current month
function getMonth(date){
	var month = parseInt(date.split("-")[1])
	,	result = "Jan"

	switch(month){
		case 1 : 
			result = "Jan";
			break;
		case 2 : 
			result = "Feb";
			break;
		case 3 : 
			result = "Mar";
			break;
		case 4 : 
			result = "Apr";
			break;
		case 5 : 
			result = "May";
			break;
		case 6 : 
			result = "Jun";
			break;		
		case 7 : 
			result = "Jul";
			break;
		case 8 : 
			result = "Aug";
			break;
		case 9 : 
			result = "Sep";
			break;
		case 10 : 
			result = "Oct";
			break;
		case 11 : 
			result = "Nov";
			break;
		case 12 : 
			result = "Dec";
			break;					
	}
	return result;
}

//given a date it returns the current month
function getMonthNumber(date){
	
	var	result = 1

	switch(date){
		case "01" : 
			result = 0;
			break;
		case "02" : 
			result = 1;
			break;
		case "03" : 
			result = 2;
			break;
		case "04" : 
			result = 3;
			break;
		case "05" : 
			result = 4;
			break;
		case "06" : 
			result = 5;
			break;		
		case "07" : 
			result = 6;
			break;
		case "08" : 
			result = 7;
			break;
		case "09" : 
			result = 8;
			break;
		case "10" : 
			result = 9;
			break;
		case "11" : 
			result = 10;
			break;
		case "12" : 
			result = 11;
			break;					
	}
	return result;
}
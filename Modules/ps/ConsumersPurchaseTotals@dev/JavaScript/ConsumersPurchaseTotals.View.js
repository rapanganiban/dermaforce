/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module CustomerInvite
define('ConsumersPurchaseTotals.View'
,	[
		'consumers_purchase_totals.tpl'
	,	'GlobalViews.Message.View'
		
	,	'Backbone'
	,	'jQuery'
	,	'underscore'	

	]
,	function(
		consumers_purchase_totals
	,	GlobalViewsMessageView	
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';
	return Backbone.View.extend({

		// @property {Function} template
		template: consumers_purchase_totals

	,	title: 'ConsumersPurchaseTotals'
	       	
		// @method initialize
	,	initialize: function (options)
		{			
			this.application = options.application;
			
		}	
	});	
});
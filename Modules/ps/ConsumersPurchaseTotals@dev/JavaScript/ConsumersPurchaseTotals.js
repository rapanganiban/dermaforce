/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// CustomerInvite.js
// -----------------
// Defines the CustomerInvite module (Model, Views, Router)
// @module CustomerInvite
define(
		'ConsumersPurchaseTotals'
,	[	
		'ConsumersPurchaseTotals.Router'

	,	'underscore'
	,	'Utils'
	]
,	function (
		Router
	,	_
	)
{
	'use strict';

	return	{

		/*// @property {MenuItem} MenuItems
		MenuItems: function () 
		{	
			return 	{	
				id: 'ConsumersPurchaseTotals'
			,	name: _('Consumers Agregated Totals').translate()
			,	url: 'consumerstotals'
			,	index: 6
			};
		}*/

		// @method mountToApp
		mountToApp: function (application)
		{
			return new Router(application);
		}
	};
});

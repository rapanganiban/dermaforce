/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderHistory
define('ConsumersPurchaseTotals.Router'
,	[	
		'ConsumersPurchaseTotals.View'
	,	'ConsumersPurchaseTotals.Model'	
	,	'AjaxRequestsKiller'
	,	'Backbone'	
	]
,	function (
		ConsumersPurchaseTotalsView
	,	ConsumersPurchaseTotalsModel
	,	AjaxRequestsKiller
	,	Backbone
	)
{

	'use strict';

	//@class OrderHistory.Router Router for handling orders @extend Backbone.Router
	return Backbone.Router.extend({
		//@property {Object} routes
		routes: {
			'consumerstotals': 'consumerstotals'
		,	'consumerstotals?:options': 'consumerstotals'		
		}
		//@method initialize
	,	initialize: function (application)
		{
			this.application = application;
		}

		//@method consumerstotals view order's detail @param {String} id
	,	consumerstotals: function (recordtype, id)
		{
			var model = new ConsumersPurchaseTotalsModel()
			,	view = new ConsumersPurchaseTotalsView({
					application: this.application				
				,	model: model
				});

			model
				.on('change', view.showContent, view)
				.fetch({
						killerId: AjaxRequestsKiller.getKillerId()
				});
		}
	});
});
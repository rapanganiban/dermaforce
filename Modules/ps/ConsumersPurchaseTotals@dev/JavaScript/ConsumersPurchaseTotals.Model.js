/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module OrderHistory
define('ConsumersPurchaseTotals.Model'
,	[	'Order.Model'
	,	'OrderFulfillment.Collection'
	,	'ReturnAuthorization.Collection'
	,	'OrderLine.Collection'
	,	'Transaction.Collection'
	]
,	function (
		OrderModel
	,	OrderFulfillmentsCollection
	,	ReturnAuthorizationCollection
	,	OrderLineCollection
	,	TransactionCollection
	)
{
	'use strict';

	//@class OrderHistory.Model Model for showing information about past orders @extend Order.Model
	return Backbone.Model.extend(
    {
     	urlRoot: _.getAbsoluteUrl('services/ConsumersPurchaseTotals.Service.ss')
    })
	
});
